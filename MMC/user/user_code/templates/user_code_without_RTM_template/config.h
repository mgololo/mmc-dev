#ifndef CONFIG_H_ 
#define CONFIG_H_ 

/*********************************************
 * PRODUCT INFORMATION 
 *********************************************/
#define IPMI_MSG_MANU_ID_LSB           0x60
#define IPMI_MSG_MANU_ID_B2            0x00
#define IPMI_MSG_MANU_ID_MSB           0x00

#define IPMI_MSG_PROD_ID_LSB           0x35
#define IPMI_MSG_PROD_ID_MSB           0x12

#define MMC_FW_REL_MAJ                 3
#define MMC_FW_REL_MIN                 0

#define FRU_NAME                       "MMC Temp."

/*********************************************
 * CUSTOM ADDRESSES FOR BENCHTOP 
 *********************************************/
#define CUSTOM_ADDR_LIST   \
    ADDR(0xFF, UNCONNECTED, UNCONNECTED, UNCONNECTED) 

/*********************************************
 * PAYLOD CONFIGURATION 
 ********************************************/

#define POWER_ON_SEQ   

#define POWER_OFF_SEQ


#endif
