/*
 * sensors.h
 *
 * Created: 26/03/2015 22:22:20
 *  Author: jumendez
 */ 


#ifndef SENSORS_H_
#define SENSORS_H_
/************************************************************************/
//	19:35 11:27		+-------+-------+----------+--------+
//    {       //                                               | resistor div. |   1/Rsense(mOhm)  |  --> note that the dividers from 3.3V and VADJ L8/L12 have been removed
//      //---+-----+-----+-----+----+----+-------+-------+----------+--------+
//		//bus|addr |cfg_a|cfg_b|exp1|exp2|vscale1|vscale2| iscale1  |iscale2 |
//		//---+-----+-----+-----+----+----+-------+-------+----------+--------+
//	    { 1  , 0x4D, 0x5F, 0x5E, 5.0, 1.8,    2.0,    1.0, (0.0/3.0),  1.0   }, // 0 :: 5.0V     :: 1.8V   --> masked the  5V I meas. (inaccurate)
//	    { 1  , 0x4C, 0x5F, 0x5E, 1.0, 1.5,    1.0,    1.0,      1.0 ,  1.0   }, // 1 :: 1.0V     :: 1.5V
//	    { 2  , 0x4D, 0x5F, 0x5E,12.0, 3.3,   4.98,    1.0, (0.0/5.0),  1.0   }, // 2 :: 12V              :: 3.3 MP --> masked the 12V I meas. (inaccurate)
//	    { 2  , 0x4C, 0x5F, 0x5E, 1.2, 1.0,    1.0,    1.0,      1.0 ,  1.0   }, // 3 :: 1.2V GTX :: 1.0V GTX
//	    { 2  , 0x4E, 0x5F, 0x5E, 3.3, 2.5,    1.0,    1.0, (1.0/3.0),  1.0   }, // 4 :: 3.3V     :: 2.5V
//	    { 4  , 0x4C, 0x5F, 0x5E, 2.5, 2.5,    1.0,    1.0,      1.0 ,  1.0   }, // 5 :: VADJ_L8  :: VADJ_L12    x2
//	    { 5  , 0x4C, 0x58, 0x59, 1.8, 0.0,    1.0,    0.0,      1.0 ,  1.0   }, // 6 :: 1.8V GTX :: FPGA_TEMP
//		//---+-----+-----+-----+----+----+-------+-------+----------+--------+
//    };
//
/* // config a/b cases
// 0x1F => case 7, V1,V2,V3,V4 -> auto trigger // 0x5F => case 7, V1,V2,V3,V4 -> single trigger
// 0x1E => case 6, V1-V2,V3-V4 -> auto trigger // 0x5E => case 6, V1-V2,V3-V4 -> single trigger
// 0x18 => case 0, V1,T2       -> auto trigger // 0x58 => case 0, V1,T2       -> single trigger
// 0x19 => case 1, I1,T2       -> auto trigger // 0x59 => case 1, I1,T2       -> single trigger
                                                                     */
/************************************************************************/		

//Sensors ID
#define TEMPERATURE_SENSOR  2	//First user sensor id: 2 (0: Hotswap, 1: 12V)

#define USER_SENSOR_CNT		6	

#define SHT21_temperature					\
{											\
	{										\
		sensor_number: 2,	\
		init_time: MP_PRESENT,				\
		name: "SHT21-Temp",					\
		i2c_addr: 0x40,						\
		p1: POINT(0, -46.85),				\
		p2: POINT(100, 22.05),				\
		upper_non_rec: 60,					\
		upper_critical: 50,					\
		upper_non_critical: 30,				\
		lower_non_critical: 10,				\
		lower_critical: 5,					\
		lower_non_rec: 0,					\
		pre_user_func: setMUXchan,	\
	}										\
}

#define LTC2990_temperature					\
{											\
	{										\
		sensor_number: 3,	\
		init_time: MP_PRESENT,				\
		name: "FPGA-Temp",					\
		i2c_addr: 0x4c,						\
		p1: POINT(1, 0.5),				\
		p2: POINT(100, 50),				\
		upper_non_rec: 90,					\
		upper_critical: 70,					\
		upper_non_critical: 50,				\
		lower_non_critical: 10,				\
		lower_critical: 5,					\
		lower_non_rec: 0,					\
		pre_user_func: setMUXchan,	\
	}										\
}

#define LTC2990_voltage				\
{											\
	{										\
		sensor_number: 4,	\
		init_time: PP_PRESENT,				\
		name: "1.0V",					\
		i2c_addr: 0x4c,						\
		opMode: 0x5f,						\
		channelAddress: 0x8,						\
		p1: POINT(0, 0),				\
		p2: POINT(1, 0.01953152),				\
		upper_non_rec: 3,					\
		upper_critical: 2,					\
		upper_non_critical: 2,				\
		lower_non_critical: 0,				\
		lower_critical: 0,					\
		lower_non_rec: 0,					\
		pre_user_func: setMUXchan,	\
	},										\
	{										\
		sensor_number: 5,	\
		init_time: PP_PRESENT,				\
		name: "1.5V",					\
		i2c_addr: 0x4c,						\
		opMode: 0x5f,						\
		channelAddress: 0xc,						\
		p1: POINT(0, 0),				\
		p2: POINT(1, 0.01953152),				\
		upper_non_rec: 3,					\
		upper_critical: 2,					\
		upper_non_critical: 2,				\
		lower_non_critical: 0,				\
		lower_critical: 0,					\
		lower_non_rec: 0,					\
		pre_user_func: setMUXchan,	\
	},										\
	{										\
		sensor_number: 6,	\
		init_time: PP_PRESENT,				\
		name: "3.3V",					\
		i2c_addr: 0x4e,						\
		opMode: 0x5f,						\
		channelAddress: 0x8,						\
		p1: POINT(0, 0),				\
		p2: POINT(1, 0.01953152),				\
		upper_non_rec: 100,					\
		upper_critical: 90,					\
		upper_non_critical: 80,				\
		lower_non_critical: 2,				\
		lower_critical: 1,					\
		lower_non_rec: 0,					\
		pre_user_func: setMUXchan,	\
	},										\
	{										\
		sensor_number: 7,	\
		init_time: PP_PRESENT,				\
		name: "2.5V",					\
		i2c_addr: 0x4e,						\
		opMode: 0x5f,						\
		channelAddress: 0xc,						\
		p1: POINT(0, 0),				\
		p2: POINT(1, 0.01953152),				\
		upper_non_rec: 4,					\
		upper_critical: 4,					\
		upper_non_critical: 4,				\
		lower_non_critical: 1,				\
		lower_critical: 0,					\
		lower_non_rec: 0,					\
		pre_user_func: setMUXchan,	\
	}										\
}


unsigned char setMUXchan(unsigned char sensor_number);

#endif /* SENSORS_H_ */