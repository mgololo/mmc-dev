#ifndef FRU_INFO_H 
#define FRU_INFO_H 

#define FORCE_WRITE_FRU

/*********************************************
 * Common defines
 *********************************************/
#define LANG_CODE               ENGLISH 
#define FRU_FILE_ID             "CoreFRU" 

/*********************************************  
 * Board information area                       
 *********************************************/ 
#define BOARD_INFO_AREA_ENABLE
#define PRODUCT_INFO_AREA_ENABLE
#define MULTIRECORD_AREA_ENABLE

#define PRODUCT_MANUFACTURER           "UF"
#define PRODUCT_NAME                   "CMS Endcap Muon Trigger"
#define PRODUCT_PN                     "0001"
#define PRODUCT_VERSION                "v.1.0"
#define PRODUCT_SN                     "0001"
#define PRODUCT_ASSET_TAG              "No asset tag"

/*********************************************  
 * Board information area                    
 *********************************************/ 
#define BOARD_MANUFACTURER             "UF"
#define BOARD_NAME                     "MTF7"
#define BOARD_SN                       "0001"
#define BOARD_PN                       "0001"

/*********************************************  
 * AMC: Point to point connectivity record      
 *********************************************/ 
#define PORTCONF_CNT	                1

#define PORTCONF_LIST                                   \
    {                                                   \
        {                                               \
            port: 4,                                    \
            protocol: PCIE,                         \
            extension: GEN1_NO_SSC,                         \
            matching: 0x01,                         \
        }                                               \
    }
	
/********************************************** 
 * PICMG: Module current record 
 **********************************************/
#define MODULE_CURRENT_RECORD            current_in_ma(6000)    //      current_in_ma(max_current_in_mA) 

#endif 
