/*
 * sensors.c
 *
 * Created: 27/03/2015 12:37:17
 *  Author: jumendez
 */ 
#include <avr/io.h>

#include "../sdr.h"
#include "../a2d.h"
#include "../i2csw.h"

#include "../avrlibdefs.h"
#include "../avrlibtypes.h"

#include "../ekeying.h"

#include "sensors.h"

/** Sensor initialization */
void sensor_init_user(){
	/** SENSOR1 init */
}

/** Sensor are polled (Every 100 ms) */
void sensor_monitoring_user(){ 
	/** set_sensor_value(SENSOR1,0x00); */
}