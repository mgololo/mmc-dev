/*
 * sensors.h
 *
 * Created: 26/03/2015 22:22:20
 *  Author: jumendez
 */ 


#ifndef SENSORS_H_
#define SENSORS_H_

/** Sensors ID */
	/** #define SENSOR1		2	// Hot swap sensor (Id: 0) - 12V sensor (Id: 1)	*/
	/** #define SENSOR2		4	// RTM Hotswap sensor (Id: Last AMC ID +1) */
	
/** SDR repository */
	/*
	#define AMC0_RECORD		{																\
		0x00,								(Record ID: Filled by init)						\
		0x00,								(Record ID: Filled by init)						\
		0x51,								(SDR Version: 51h)								\
		0x01,								(Record type: 01h - full sensor)				\
		0x33,								(Record length: Filled by init)					\
		0x00,								(Sensor owner ID: Filled by init)				\
		0x00,								(Sensor owner LUN: 00h)							\
		SENSOR1,							(Sensor number)									\
		0xc1,								(Entity ID: C1h - AMC)							\
		0x00,								(Entity Instance)								\
		0x7F,								(Sensor Initialization)							\
		0xFC,								(Sensor capabilities)\
		TEMPERATURE,						(Sensor type)									\
		0x01,								(Event/Reading type code)						\
		0xFF,								(Assertion event mask (LSB))					\
		0x7F,								(Assertion event mask (MSB))					\
		0xFF,								(Deassertion event mask (LSB))					\
		0x7F,								(Deassertion event mask (MSB))					\
		0x00,								(Settable / Readable threshold mask (LSB))		\
		0x00,								(Settable / Readable threshold mask (MSB))\
		0x80,								(Sensor unit: 80h - 2's complement (signed))	\
		0x01,								(Sensor unit 2 - Base unit)						\
		0x00,								(Sensor unit 3 - Modifier unit)					\
		0x00,								(Linearization: 00h - Linear)					\
		0x01,								(M)												\
		0x01,								(M, Tolerance)									\
		0x00,								(B)												\
		0x25,								(B, Accuracy)									\
		0x88,								(Accuracy, Accuracy exp)						\
		0x00,								(R exp, B exp)									\
		0x07,								(Analog characteristic flag)					\
		25,									(Normal reading)								\
		50,									(Normal max)									\
		20,									(Normal min)									\
		0x7F,								(Maximum reading)								\
		0x80,								(Minimum reading)								\
		85,									(Upper non-recoverable threshold)				\
		75,									(Upper critical threshold)						\
		65,									(Upper non-critical threshold)					\
		-20,								(Lower non-recoverable threshold)				\
		-10,								(Lower critical threshold)						\
		0,									(Lower non critical threshold)					\
		0x02,								(Positive-going hysteresis)						\
		0x02,								(Negative-going hysteresis)						\
		0x00,								(reserved: 00h)									\
		0x00,								(reserved: 00h)									\
		0x00,								(reserved for OEM use)							\
		0xC8,								(ID string type/length)							\
		'L','M','8','2',' ','I','C','1'		(ID String)										\
	}
	*/
	
	/*
	#define RTM0_RECORD		{																\
		0x00,								(Record ID: Filled by init)						\
		0x00,								(Record ID: Filled by init)						\
		0x51,								(SDR Version: 51h)								\
		0x01,								(Record type: 01h - full sensor)				\
		0x33,								(Record length: Filled by init)					\
		0x00,								(Sensor owner ID: Filled by init)				\
		0x00,								(Sensor owner LUN: 00h)							\
		SENSOR2,							(Sensor number)									\
		0xc0,								(Entity ID: C0h - RTM)							\
		0x00,								(Entity Instance)								\
		0x7F,								(Sensor Initialization)							\
		0xFC,								(Sensor capabilities)\
		TEMPERATURE,						(Sensor type)									\
		0x01,								(Event/Reading type code)						\
		0xFF,								(Assertion event mask (LSB))					\
		0x7F,								(Assertion event mask (MSB))					\
		0xFF,								(Deassertion event mask (LSB))					\
		0x7F,								(Deassertion event mask (MSB))					\
		0x00,								(Settable / Readable threshold mask (LSB))		\
		0x00,								(Settable / Readable threshold mask (MSB))\
		0x80,								(Sensor unit: 80h - 2's complement (signed))	\
		0x01,								(Sensor unit 2 - Base unit)						\
		0x00,								(Sensor unit 3 - Modifier unit)					\
		0x00,								(Linearization: 00h - Linear)					\
		0x01,								(M)												\
		0x01,								(M, Tolerance)									\
		0x00,								(B)												\
		0x25,								(B, Accuracy)									\
		0x88,								(Accuracy, Accuracy exp)						\
		0x00,								(R exp, B exp)									\
		0x07,								(Analog characteristic flag)					\
		25,									(Normal reading)								\
		50,									(Normal max)									\
		20,									(Normal min)									\
		0x7F,								(Maximum reading)								\
		0x80,								(Minimum reading)								\
		85,									(Upper non-recoverable threshold)				\
		75,									(Upper critical threshold)						\
		65,									(Upper non-critical threshold)					\
		-20,								(Lower non-recoverable threshold)				\
		-10,								(Lower critical threshold)						\
		0,									(Lower non critical threshold)					\
		0x02,								(Positive-going hysteresis)						\
		0x02,								(Negative-going hysteresis)						\
		0x00,								(reserved: 00h)									\
		0x00,								(reserved: 00h)									\
		0x00,								(reserved for OEM use)							\
		0xC8,								(ID string type/length)							\
		'L','M','8','2',' ','I','C','1'		(ID String)										\
	}
	*/
	
#endif /* SENSORS_H_ */