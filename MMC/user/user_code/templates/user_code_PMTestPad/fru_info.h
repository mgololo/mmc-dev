#ifndef FRU_INFO_H 
#define FRU_INFO_H 

#define FORCE_WRITE_FRU

/*********************************************
 * Common defines
 *********************************************/
#define LANG_CODE               ENGLISH 
#define FRU_FILE_ID             "CoreFRU" 

/*********************************************  
 * Board information area                       
 *********************************************/ 
#define BOARD_INFO_AREA_ENABLE
#define PRODUCT_INFO_AREA_ENABLE
#define MULTIRECORD_AREA_ENABLE

#define PRODUCT_MANUFACTURER           "CERN"
#define PRODUCT_NAME                   "PM TestPad - MMC"
#define PRODUCT_PN                     "0001"
#define PRODUCT_VERSION                "v.3.0"
#define PRODUCT_SN                     "0001"
#define PRODUCT_ASSET_TAG              "No asset tag"

/*********************************************  
 * Board information area                    
 *********************************************/ 
#define BOARD_MANUFACTURER             "CERN"
#define BOARD_NAME                     "PM TestPad - MCH backplane"
#define BOARD_SN                       "0001"
#define BOARD_PN                       "0002"

/*********************************************  
 * AMC: Point to point connectivity record      
 *********************************************/ 
// No AMC point to point connectivity

/********************************************** 
 * PICMG: Module current record 
 **********************************************/
#define MODULE_CURRENT_RECORD            current_in_ma(7500)    //      current_in_ma(max_current_in_mA) 

#endif