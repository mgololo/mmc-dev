#ifndef CONFIG_H_ 
#define CONFIG_H_ 

/*********************************************
 * General config
 *********************************************/
#define ENABLE_OEM

void user_init();
#define USER_INIT_FUNC		user_init

#define FORCE_HANDLE_VALUE	HANDLE_SWITCH_CLOSED

/*********************************************
 * PRODUCT INFORMATION 
 *********************************************/
#define IPMI_MSG_MANU_ID_LSB           0x60
#define IPMI_MSG_MANU_ID_B2            0x00
#define IPMI_MSG_MANU_ID_MSB           0x00

#define IPMI_MSG_PROD_ID_LSB           0x35
#define IPMI_MSG_PROD_ID_MSB           0x12

#define MMC_FW_REL_MAJ                 2
#define MMC_FW_REL_MIN                 0

#define FRU_NAME                       "PM TestPad"

/*********************************************
 * CUSTOM ADDRESSES FOR BENCHTOP 
 *********************************************/
#define CUSTOM_ADDR_LIST   \
    ADDR(0xFF, UNCONNECTED, UNCONNECTED, UNCONNECTED) 

/*********************************************
 * PAYLOAD CONFIGURATION 
 ********************************************/

#define POWER_ON_SEQ   //Nothing to do

#define POWER_OFF_SEQ  //Nothing to do

#endif
