/*
 * config.h
 *
 * Created: 19/03/2015 18:20:14
 *  Author: jumendez
 */ 


#ifndef CONFIG_H_
#define CONFIG_H_

/** ENABLE CONTROLLER SPECIFIC COMMANDS */
	//#define ENABLE_CONTROLLER_SPECIFIC

/** ENABLE OEM COMMANDS */
	#define ENABLE_OEM

/** Product information */
	#define IPMI_MSG_MANU_ID_LSB 0x2E   //NOTE: Manufacturer identification is handled by http://www.iana.org/assignments/enterprise-numbers
	#define IPMI_MSG_MANU_ID_B2  0xA1       //CERN IANA ID = 0x000060
	#define IPMI_MSG_MANU_ID_MSB 0x00

	#define IPMI_MSG_PROD_ID_LSB 0x35
	#define IPMI_MSG_PROD_ID_MSB 0x12

	#define MMC_FW_REL_MAJ       2                  // major firmware release version ( < 128 )
	#define MMC_FW_REL_MIN       0                  // minor firmware release version ( < 256 )

	#define FRU_NAME                        "AMC40"


/** USER GPIO INITIALIZATION */
	#define GPIO9_DIR		OUTPUT		// GPIO9 (PE6) -> JTAG_CONTROL[0]
	#define GPIO12_DIR		OUTPUT		// GPIO12 (PE7) -> JTAG_CONTROL[1]

/** PAYLOD CONFIGURATION */
	//Define power on sequence (Mandatory)
	#define POWER_ON_SEQ												\
		SET_PAYLOAD_SIGNAL(LOCAL_DCDC_ENABLE)							\
		SET_PAYLOAD_SIGNAL(LOCAL_REG_ENABLE)							\
		WAIT_FOR_HIGH(LOCAL_FPGA1_INIT_DONE, 700, PON_ERROR)			\
		CLEAR_PAYLOAD_SIGNAL(LOCAL_RESET_FPGA)							\
		DELAY(200)														\
		SET_PAYLOAD_SIGNAL(LOCAL_RESET_FPGA)							\
		CLEAR_PAYLOAD_SIGNAL(LOCAL_RELOAD_FPGA)							\
		DELAY(200)														\
		SET_PAYLOAD_SIGNAL(LOCAL_RELOAD_FPGA)							\
		CLEAR_PAYLOAD_SIGNAL(GPIO_9)										\
		CLEAR_PAYLOAD_SIGNAL(GPIO_12)									\

	//Define power OFF sequence (Mandatory)	
	#define POWER_OFF_SEQ												\
		CLEAR_PAYLOAD_SIGNAL(LOCAL_DCDC_ENABLE)							\
		CLEAR_PAYLOAD_SIGNAL(LOCAL_REG_ENABLE)						
	
	//Define reboot sequence (Optional)
	#define REBOOT_SEQ													\
		CLEAR_PAYLOAD_SIGNAL(LOCAL_DCDC_ENABLE)							\
		CLEAR_PAYLOAD_SIGNAL(LOCAL_REG_ENABLE)							\
		DELAY(500)														\
		SET_PAYLOAD_SIGNAL(LOCAL_DCDC_ENABLE)							\
		SET_PAYLOAD_SIGNAL(LOCAL_REG_ENABLE)							\
		WAIT_FOR_HIGH(LOCAL_FPGA1_INIT_DONE, 700, PON_ERROR)			\
		CLEAR_PAYLOAD_SIGNAL(LOCAL_RESET_FPGA)							\
		DELAY(200)														\
		SET_PAYLOAD_SIGNAL(LOCAL_RESET_FPGA)							\
		CLEAR_PAYLOAD_SIGNAL(LOCAL_RELOAD_FPGA)							\
		DELAY(200)														\
		SET_PAYLOAD_SIGNAL(LOCAL_RELOAD_FPGA)
	
	//Define warm reset sequence (Optional)
	#define WARM_RESET_SEQ												\
		CLEAR_PAYLOAD_SIGNAL(LOCAL_RESET_FPGA)							\
		DELAY(200)														\
		SET_PAYLOAD_SIGNAL(LOCAL_RESET_FPGA)							\
		CLEAR_PAYLOAD_SIGNAL(LOCAL_RELOAD_FPGA)							\
		DELAY(200)														\
		SET_PAYLOAD_SIGNAL(LOCAL_RELOAD_FPGA)

	//Define cold reset sequence (Optional)	
	#define COLD_RESET_SEQ												\
		CLEAR_PAYLOAD_SIGNAL(LOCAL_RESET_FPGA)							\
		DELAY(200)														\
		SET_PAYLOAD_SIGNAL(LOCAL_RESET_FPGA)							\
		CLEAR_PAYLOAD_SIGNAL(LOCAL_RELOAD_FPGA)							\
		DELAY(200)														\
		SET_PAYLOAD_SIGNAL(LOCAL_RELOAD_FPGA)


/** LED CONFIGURATION : AMC40 - NO MMC User's led */

		
/** CUSTOM ADDRESSES FOR BENCHTOP */
#define CUSTOM_ADDR_LIST									\
	ADDR(0xF0, UNCONNECTED, UNCONNECTED, UNCONNECTED)		\
	ADDR(0xFF, POWERED, POWERED, POWERED)
	
#endif /* CONFIG_H_ */