/*
 * sensors.h
 *
 * Created: 26/03/2015 22:22:20
 *  Author: jumendez
 */ 


#ifndef SENSORS_H_
#define SENSORS_H_

/** Sensor drivers */
#define USER_SENSOR_CNT		0
/*
#define LM75								\
{											\
	{										\
		sensor_number: 2,	\
		init_time: MP_PRESENT,				\
		name: "Temperature",					\
		i2c_addr: 0x48,						\
		p1: POINT(1, 0.5),				\
		p2: POINT(100, 50),				\
		upper_non_rec: 90,					\
		upper_critical: 70,					\
		upper_non_critical: 50,				\
		lower_non_critical: 10,				\
		lower_critical: 5,					\
		lower_non_rec: 0,					\
	}										\
}
*/
#endif /* SENSORS_H_ */