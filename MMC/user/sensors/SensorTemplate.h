/*
* SensorTemplate.h
*
* Created: 2015/12/16 18:48:15
* Author: Julian Mendez <julian.mendez@cern.ch>
*
* HowTo: 
*	-> Replace all SensorTemplate by the name of the sensors (you can use replace all funtion)
*	-> Add the sensor specific variables used by the driver in the struct
*	-> Modify the default SDR values
*/

#ifdef REGISTER_SENSOR
	#ifdef SensorTemplate
		SENSOR_DECLARATION(update_SensorTemplate_value, init_SensorTemplate_sdr, get_SensorTemplate_sdr_byte, set_SensorTemplate_threshold, 1);
	#endif
#else

	#ifndef SensorTemplate_H_
	#define SensorTemplate_H_

		typedef struct SensorTemplate_s{

			/** Generic information */
			unsigned char sensor_location;  /** AMC (default) or RTM */
			unsigned char init_time; /** Execute init when MP (default) or PP is present */
			unsigned char is_init; 
			unsigned char sensor_number;
			char *name;

			/** Raw to data computing */
			signed short M, B;
			signed char Bexp, Rexp;
			float p1[2];
			float p2[2];

			/** Threshold information */
			unsigned char upper_non_rec;
			unsigned char upper_critical;
			unsigned char upper_non_critical;
			unsigned char lower_non_critical;
			unsigned char lower_critical;
			unsigned char lower_non_rec;

			/** Data used to fill SDR */
			unsigned char id;
			unsigned char entity_inst;
			unsigned char owner_id;

			/** User specific function */	
			unsigned char  (*pre_user_func)(unsigned char sensor_number);
			unsigned char  (*post_user_func)(unsigned char sensor_number);

			/** Sensor specific data */
			
				/** E.g.:
					unsigned char i2c_addr;
				*/
				
		} SensorTemplate_t; 

		unsigned char update_SensorTemplate_value();  
		unsigned char get_SensorTemplate_sdr_byte(signed char *byte, unsigned char len, unsigned char sensor_id, unsigned char offset); 
		unsigned char init_SensorTemplate_sdr(unsigned char sdr_id, unsigned char entity_inst, unsigned char owner_id); 
		unsigned char set_SensorTemplate_threshold(unsigned char sensor_id, unsigned char mask, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr);

		#define SensorTemplate_SDR { \
			0x00, 	  \
			0x00, 	  \
			0x51, 	  \
			0x01, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0xc1, 	  \
			0x00, 	  \
			0x7f, 	  \
			0xFC, 	  \
			TEMPERATURE, 	  \
			0x01, 	  \
			0xff, 	  \
			0x7f, 	  \
			0xff, 	  \
			0x7f, 	  \
			0x3f, 	  \
			0x3f, 	  \
			0x80, 	  \
			0x1, 	  \
			0x00, 	  \
			0x00, 	  \
			0x01, 	  \
			0x01, 	  \
			0x00, 	  \
			0x25, 	  \
			0x88, 	  \
			0x00, 	  \
			0x07, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x7F, 	  \
			0x80, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x02, 	  \
			0x02, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0xC0 	  \
		}

	#endif 
#endif 
