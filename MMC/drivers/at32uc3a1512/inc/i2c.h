/*
 * i2c.h
 *
 * Created: 09/11/2015 22:01:58
 *  Author: jumendez
 */ 


#ifndef I2C_H_
#define I2C_H_

#define I2C_SUCCESS 0
#define I2C_ERROR 1

void i2cInit();
void i2cSetSlaveReceiveHandler(void (*i2cSlaveRx_func)(unsigned char receiveDataLength, unsigned char* recieveData));
void i2cSetSlaveTransmitHandler(void (*i2cSlaveTx_func)(unsigned char receiveDataLength, unsigned char* recieveData));

/****************************
 Slave functions prototypes
 ****************************/
void i2c_slave_rx ( unsigned char pByteIn );
unsigned char i2c_slave_tx(void);
void i2c_slave_stop(void);

#endif /* I2C_H_ */