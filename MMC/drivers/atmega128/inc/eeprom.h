//*****************************************************************************
// Copyright (C) 2010 CPPM(Centre Pysique des Particules de Marseille) 
//
//File Name	: eeprom.h
// 
// Title		: eeprom_cppm header file
// Revision		: 2.0
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Bompard Frederic, CPPM
// Modified by  : Markus Joos (markus.joos@cern.ch)
//
// Description  : EEPROM Management
//					
//
//*****************************************************************************

#ifndef EEPROM_CPPM_H
#define EEPROM_CPPM_H

unsigned char EEPROM_read(unsigned short uiAddress);
void EEPROM_write(unsigned short uiAddress, unsigned char ucData);
void write_hpm_bootloader_force_flag();
void write_hpm_app_force_flag();

#endif
