/*! \file iodeclaration.h \ IO declaration - Header file */
//*****************************************************************************
//
// File Name	: 'iodeclaration.h'
// Title		: Pinout definition
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************

#ifndef IODECLARATION_H_
#define IODECLARATION_H_

typedef struct {
	unsigned char port;
	unsigned char pin;
	unsigned char active;
	unsigned char inactive;
}signal_t;

void init_port(void);

#endif /* IODECLARATION_H_ */