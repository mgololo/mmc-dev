/*
 * common.h
 *
 * Created: 14/10/2015 13:54:10
 *  Author: jumendez
 */ 


#ifndef COMMON_H_
#define COMMON_H_

	#include <avr/io.h>
	#include <avr/interrupt.h>	
	#include <avr/sleep.h>
	#include <avr/wdt.h>	
	
#endif /* COMMON_H_ */