/*! \file timer.c \ Timer functions */
//*****************************************************************************
//
// File Name	: 'timer.c'
// Title		: Timer functions
// Author		: Unknown
// Modified by  : Markus joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************
#include <avr/interrupt.h>

#include "../../../application/inc/led.h"
#include "../inc/global.h"
#include "../inc/avrlibdefs.h"

#include "../inc/timer.h"
// Program ROM constants
// the pre-scale division values stored in order of timer control register index
// STOP, CLK, CLK/8, CLK/64, CLK/256, CLK/1024
//unsigned short __attribute__ ((progmem)) const TimerPrescaleFactor[] = {0,1,8,64,256,1024};

voidFuncPtr timer_10ms_callback;
voidFuncPtr timer_100ms_callback;

unsigned char timerEvent = 0, ledEvent = 0;
int LedTimerCnter = 0, EventTimerCnter = 0;

// Delay for a minimum of <us> microseconds
// The time resolution is dependent on the time the loop takes
// e.g. with 4Mhz and 5 cycles per loop, the resolution is 1.25 us 
//***********************/
void delay_us(unsigned short time_us)     //Called from ipmi_if.c and fru.c
//***********************/
{
	unsigned short delay_loops;
	register unsigned short i;     

	// one loop takes 5 cpu cycles 
	delay_loops = (time_us + 3) / 22 * 3 * CYCLES_PER_US; // +3 for rounding up (dirty)  //MJ: "22 * 3" is the result of a manual calibration of the delay

	for (i=0; i < delay_loops; i++)
	    asm volatile("nop");          //MJ: Without the "nop" there is the risk that the loop gets removed by the optimizer
}

void delay_ms(unsigned short time_ms)     //Called from ipmi_if.c and fru.c
//***********************/
{
	unsigned short delay_loops;
	register unsigned short i,j;

	// one loop takes 5 cpu cycles
	delay_loops = (time_ms + 3) / 22 * 3 * CYCLES_PER_US; // +3 for rounding up (dirty)  //MJ: "22 * 3" is the result of a manual calibration of the delay

	for(j=0; j<1000; j++){
		for (i=0; i < delay_loops; i++)
			asm volatile("nop");          //MJ: Without the "nop" there is the risk that the loop gets removed by the optimizer
	}
}

//*****************/
void timerInit(void)     //Called from mmc_main.c
//*****************/
{
	timerEvent = 0;	
	LedTimerCnter = 0;
	EventTimerCnter = 0;
	
	timer_10ms_callback = 0;
	timer_100ms_callback = 0;
	
    // Initialize timer 0
    outb(TCCR0, (inb(TCCR0) | CTC_MODE));   // set set CTC mode
	timerSetPrescaler(TIMER0PRESCALE);	    // set pre-scaler
	outb(TCNT0, 0);							// reset TCNT0
    outb(OCR0, 0x8C);                       // set value to output compare register
	sbi(TIMSK, OCIE0);						// enable output compare match interrupt
}


//*********************************/
void timerSetPrescaler(unsigned char prescale)   //Only used locally in this file  //MJ: Can this function be in-lined?
//*********************************/
{
	// set pre-scaler on timer 0
	outb(TCCR0, (inb(TCCR0) & ~TIMER_PRESCALE_MASK) | prescale);
}

void timer_10ms_attach(void (*userFunc)(void)){
	timer_10ms_callback = userFunc;
}

void timer_100ms_attach(void (*userFunc)(void)){
	timer_100ms_callback = userFunc;
}

void timer_10ms_disable(){
	timer_10ms_callback = 0;
}

void timer_100ms_disable(){
	timer_100ms_callback = 0;
}

// Interrupt handler for tcnt0 overflow interrupt
TIMER_INTERRUPT_HANDLER(TIMER0_COMP_vect)
{
		
	LedTimerCnter++;
	EventTimerCnter++;
	
	if (LedTimerCnter >= 9){
		LedTimerCnter = 0;
		if(timer_10ms_callback) timer_10ms_callback();
	}
	
	if (EventTimerCnter >= 100){
		if(timer_100ms_callback) timer_100ms_callback();
		EventTimerCnter = 0;
	}
}