/*
 * timer.c
 *
 * Created: 16/10/2015 09:15:30
 *  Author: jumendez
 */ 
#include <asf.h>

#include "../../drivers.h"

typedef void (*voidFuncPtr)(void);

voidFuncPtr timer_10ms_callback;
voidFuncPtr timer_100ms_callback;

void delay_ms(unsigned short time_ms){
	cpu_delay_ms(time_ms,CONFIG_PLL0_MUL*BOARD_OSC0_HZ);
}

void delay_us(unsigned short time_us){
	cpu_delay_us(time_us,CONFIG_PLL0_MUL*BOARD_OSC0_HZ);
}

void timer_100ms_attach(void (*userFunc)(void)){
	timer_100ms_callback = userFunc;
}

void timer_10ms_attach(void (*userFunc)(void)){
	timer_10ms_callback = userFunc;
}

int ms100_EventTimerCnter = 0, ms10_EventTimerCnter = 0;

ISR(tc_callback,AVR32_TC0_IRQ0,0){
	tc_read_sr(&AVR32_TC0, 0);	
	
	ms10_EventTimerCnter++;
	ms100_EventTimerCnter++;
	
	if (ms10_EventTimerCnter >= 9){
		ms10_EventTimerCnter = 0;
		if(timer_10ms_callback) timer_10ms_callback();
	}
	
	if (ms100_EventTimerCnter >= 100){
		ms100_EventTimerCnter = 0;
		if(timer_100ms_callback) timer_100ms_callback();
	}
}

/**
 * \brief TC Initialization
 *
 * Initializes and start the TC module with the following:
 * - Counter in Up mode with automatic reset on RC compare match.
 * - fPBA/8 is used as clock source for TC
 * - Enables RC compare match interrupt
 * \param tc Base address of the TC module
 */
static void tc_init(volatile avr32_tc_t *tc)
{
	// Options for waveform generation.
	static const tc_waveform_opt_t waveform_opt = {		
		.channel  = 0,									// Channel selection.		
		.bswtrg   = TC_EVT_EFFECT_NOOP,					// Software trigger effect on TIOB.		
		.beevt    = TC_EVT_EFFECT_NOOP,					// External event effect on TIOB.		
		.bcpc     = TC_EVT_EFFECT_NOOP,					// RC compare effect on TIOB.		
		.bcpb     = TC_EVT_EFFECT_NOOP,					// RB compare effect on TIOB.		
		.aswtrg   = TC_EVT_EFFECT_NOOP,					// Software trigger effect on TIOA.		
		.aeevt    = TC_EVT_EFFECT_NOOP,					// External event effect on TIOA.		
		.acpc     = TC_EVT_EFFECT_NOOP,					// RC compare effect on TIOA.
		.acpa     = TC_EVT_EFFECT_NOOP,					//RA compare effect on TIOA.
		.wavsel   = TC_WAVEFORM_SEL_UP_MODE_RC_TRIGGER,	//Waveform selection: Up mode with automatic trigger(reset) on RC compare.
		.enetrg   = 0,									// External event trigger enable.
		.eevt     = 0,									// External event selection.
		.eevtedg  = TC_SEL_NO_EDGE,						// External event edge selection.
		.cpcdis   = false,								// Counter disable when RC compare.
		.cpcstop  = false,								// Counter clock stopped with RC compare.
		.burst    = false,								// Burst signal selection.
		.clki     = false,								// Clock inversion.
		.tcclks   = TC_CLOCK_SOURCE_TC3					// Internal source clock 3, connected to fPBA / 8.
	};

	// Options for enabling TC interrupts
	static const tc_interrupt_t tc_interrupt = {
		.etrgs = 0,
		.ldrbs = 0,
		.ldras = 0,
		.cpcs  = 1, // Enable interrupt on RC compare alone
		.cpbs  = 0,
		.cpas  = 0,
		.lovrs = 0,
		.covfs = 0
	};
	
	// Initialize the timer/counter.
	tc_init_waveform(tc, &waveform_opt);

	/*
	 * Set the compare triggers.
	 * We configure it to count every 1 milliseconds.
	 * We want: (1 / (fPBA / 8)) * RC = 1 ms, hence RC = (fPBA / 8) / 1000
	 * to get an interrupt every 1 ms.
	 */
	tc_write_rc(tc, 0, (sysclk_get_pba_hz() / 8 / 1000));
	
	// configure the timer interrupt
	tc_configure_interrupts(tc, 0, &tc_interrupt);
	
	// Start the timer/counter.
	tc_start(tc, 0);
}

void timerInit(void){    /* Configure the PMC to enable the TC module */
	
	timer_100ms_callback = 0;
	timer_10ms_callback = 0;
	
	sysclk_enable_peripheral_clock(&AVR32_TC0);
	
	irqflags_t flags = cpu_irq_save();
	irq_register_handler(tc_callback,	AVR32_TC0_IRQ0, 0);
	cpu_irq_restore(flags);

	tc_init(&AVR32_TC0);
}