/*
 * io.c
 *
 * Created: 16/10/2015 09:05:09
 *  Author: jumendez
 */ 
#include <asf.h>
#include <math.h>

#include "../inc/pinout.h"

unsigned char get_signal(unsigned char id){
	return (AVR32_GPIO.port[(int)floor((id)/32)].pvr & (1 << ((id)%32))) ? HIGH:LOW;
}

void set_signal(unsigned char id){
	AVR32_GPIO.port[(int)floor((id)/32)].ovrs = (1 << ((id)%32));
}

void clear_signal(unsigned char id){
	AVR32_GPIO.port[(int)floor((id)/32)].ovrc = (1 << ((id)%32));
}

void set_signal_dir(unsigned char id, unsigned char inout){
	if(inout){
		AVR32_GPIO.port[(int)floor((id)/32)].oderc = 1 << ((id)%32);
	}else{
		AVR32_GPIO.port[(int)floor((id)/32)].oders = 1 << ((id)%32);
	}
}

void init_port(void){
	//  AVR32_GPIO register allows controlling the AVR32 micrcontrolleur GPIOs
	//  datasheet: http://www.atmel.com/Images/doc32072.pdf (p. 381)
	//  -- ODER: Output Driver Enable Register (Read/Write)
	//		[val: 0] = Input	(oders register -> set bit)
	//		[val: 1] = Output	(oderc register -> clear bit)
	//	-- GPER: GPIO Enable Register (Read/Write)
	//		[val: 0] = Deactivate pin
	//		[val: 1] = Activate pin
	//	-- OVR: Output Value Register (Read/Write)
	//	-- PVR: Pin Value Register (Read)
	//	-- PUER: Pull-Up Enable Register (Read/Write)
	
	//AVR32_GPIO.port[(int)floor((FPGA_INIT_DONE)/32)].oders = 1 << ((FPGA_INIT_DONE)%32);
	//AVR32_GPIO.port[(int)floor((FPGA_INIT_DONE)/32)].gpers = 1 << ((FPGA_INIT_DONE)%32);
	AVR32_GPIO.port[(int)floor((GA0)/32)].gpers = 1 << ((GA0)%32);
	AVR32_GPIO.port[(int)floor((GA1)/32)].gpers = 1 << ((GA1)%32);
	AVR32_GPIO.port[(int)floor((GA2)/32)].gpers = 1 << ((GA2)%32);
	AVR32_GPIO.port[(int)floor((GA_PULLUP)/32)].gpers = 1 << ((GA_PULLUP)%32);
	
	set_signal_dir(LOCAL_BLUE_LED, OUTPUT);
	set_signal_dir(LOCAL_GREEN_LED, OUTPUT);
	set_signal_dir(LOCAL_RED_LED, OUTPUT);
	set_signal_dir(PWR_GPIO_ENABLE, OUTPUT);
	
	AVR32_GPIO.port[(int)floor((LOCAL_BLUE_LED)/32)].gpers = 1 << ((LOCAL_BLUE_LED)%32);
	AVR32_GPIO.port[(int)floor((LOCAL_GREEN_LED)/32)].gpers = 1 << ((LOCAL_GREEN_LED)%32);
	AVR32_GPIO.port[(int)floor((LOCAL_RED_LED)/32)].gpers = 1 << ((LOCAL_RED_LED)%32);
	AVR32_GPIO.port[(int)floor((PWR_GPIO_ENABLE)/32)].gpers = 1 << ((PWR_GPIO_ENABLE)%32);
	
	clear_signal(PWR_GPIO_ENABLE);
}