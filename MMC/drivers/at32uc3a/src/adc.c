/*
 * adc.c
 *
 * Created: 16/10/2015 09:17:06
 *  Author: jumendez
 */ 
#include <asf.h>

#include "../inc/pinout.h"

// initialize a2d converter
//***************/
void a2dInit(void){
	adc_configure ( &AVR32_ADC );
	adc_enable ( &AVR32_ADC, PWR_GPIO_12V_CHAN );
	adc_start ( &AVR32_ADC );
}

// Perform a 8-bit conversion.
unsigned char payload_voltage_sensor(){
	unsigned char raw;
	
	long xRawValue = adc_get_value ( &AVR32_ADC, PWR_GPIO_12V_CHAN);
	adc_start ( &AVR32_ADC );
	raw = xRawValue*(255.0f/1024.0f);
	
	return (unsigned char)(raw*(0.71667));
}