/*
 * addfeatures.h
 *
 * Created: 20/10/2015 14:56:54
 *  Author: jumendez
 */ 


#ifndef ADDFEATURES_H_
#define ADDFEATURES_H_

	/* I2C ext defines */
		#define ZERO_LEN	0
		#define BYTE_LEN	1
		#define SHORT_LEN	2
	
	/* SD Card */
		unsigned char card_present();
		unsigned int card_capacity();
		int open_file(char *name, int flags);
		ssize_t write_file(int fd, char *str, size_t size);
		ssize_t read_file(int fd, char *str, size_t size);
		int close_file(int fd);
		unsigned char create_file(char *filename);
		
#endif /* ADDFEATURES_H_ */