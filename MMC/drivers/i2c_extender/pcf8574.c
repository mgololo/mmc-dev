/*! \file pcf8574.h \ Driver for pcf8574 IO extender */
//*****************************************************************************
//
// File Name	: 'pcf8574.c'
// Title		: Driver for pcf8574 IO extender
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************
#include "../drivers.h"
#include "pcf8574.h"

unsigned char get_PCF8574_port(unsigned char addr){
	unsigned char data;	
	ext_i2c_received(addr, 0, 0, 1, &data);	
	return data;
}

void set_PCF8574_port(unsigned char addr, unsigned char data){
	ext_i2c_send(addr, 0, 0, 1, &data);
}

void clear_PCF8574_bit(unsigned char addr, unsigned char pin){
	unsigned char data = get_PCF8574_port(addr);
	data &= ~(0x01 << (pin-1));
	set_PCF8574_port(addr, data);
}

void set_PCF8574_bit(unsigned char addr, unsigned char pin){
	unsigned char data = get_PCF8574_port(addr);	
	data |= (0x01 << (pin-1));		
	set_PCF8574_port(addr, data);
}

unsigned char get_PCF8574_bit(unsigned char addr, unsigned char pin){
	set_PCF8574_bit(addr, pin);
	unsigned char data = get_PCF8574_port(addr);
	data &= (0x01 << (pin-1));
	return data;	
}