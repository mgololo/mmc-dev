/*! \file pcf8574.h \ Driver for pcf8574 IO extender -Header file */
//*****************************************************************************
//
// File Name	: 'pcf8574.c'
// Title		: Driver for pcf8574 IO extender - Header file
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************


#ifndef PCF8574_H_
#define PCF8574_H_

unsigned char get_PCF8574_port(unsigned char addr);
void set_PCF8574_port(unsigned char addr, unsigned char data);
void set_PCF8574_bit(unsigned char addr, unsigned char pin);
void clear_PCF8574_bit(unsigned char addr, unsigned char pin);
unsigned char get_PCF8574_bit(unsigned char addr, unsigned char pin);

#endif /* PCF8574_H_ */