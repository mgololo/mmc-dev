/*! \file pca9698.c \ Driver for pca9698 IO extender */
//*****************************************************************************
//
// File Name	: 'pca9698.c'
// Title		: Driver for pca9698 IO extender
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 28/01/2016
// Target MCU	: Atmel AVR series
//
//*****************************************************************************
#include "../drivers.h"
#include "pca9698.h"

unsigned char get_PCA9698_port(unsigned char addr, unsigned char port){
	unsigned char data;	
	ext_i2c_received(addr, port, 1, 1, &data);	
	return data;
}

void set_PCA9698_port(unsigned char addr, unsigned char port, unsigned char data){
	ext_i2c_send(addr, port + 0x08, 1, 1, &data);
}

void clear_PCA9698_bit(unsigned char addr, unsigned char port, unsigned char pin){
	unsigned char data = get_PCA9698_port(addr, port);
	data &= ~(0x01 << (pin-1));
	set_PCA9698_port(addr, port, data);
}

void set_PCA9698_bit(unsigned char addr, unsigned char port, unsigned char pin){
	unsigned char data = get_PCA9698_port(addr, port);	
	data |= (0x01 << (pin-1));		
	set_PCA9698_port(addr, port, data);
}

unsigned char get_PCA9698_bit(unsigned char addr, unsigned char port, unsigned char pin){
	unsigned char data = get_PCA9698_port(addr, port);
	data &= (0x01 << (pin-1));
	return (data) ? 1 : 0;	
}

void set_dir_port(unsigned char addr, unsigned char port, unsigned char dir){
	unsigned char data;
	
	if(dir == OUTPUT)		data = 0x00;
	else					data = 0xFF;
	
	ext_i2c_send(addr, port + 0x18, 1, 1, &data);
}

unsigned char get_dir_port(unsigned char addr, unsigned char port){
	unsigned char data;	
	ext_i2c_received(addr, port + 0x18, 1, 1, &data);	
	return data;
}

void set_dir_pin(unsigned char addr, unsigned char port, unsigned char pin, unsigned char dir){
	unsigned char data = get_dir_port(addr, port);
	
	if(dir == OUTPUT)		data &= ~(0x01 << pin);
	else					data |= (0x01 << pin);
	
	ext_i2c_send(addr, port + 0x18, 1, 1, &data);
}