/*! \file rtm_frueditor.h \ FRU editor for RTM info - Header file */
//*****************************************************************************
//
// File Name	: 'rtm_frueditor.h'
// Title		: FRU editor for RTM info - Header file
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************

#ifndef RTM_FRUEDITOR_H_
#define RTM_FRUEDITOR_H_

//#include "user_code/user_code.h"

#ifdef USE_RTM
	void rtm_fru_header_function(void);
	void rtm_board_info_area_function(void);
	void rtm_product_info_area_function(void);
	void rtm_compatibility_record();
	unsigned char write_rtm_fru_binary();
#endif

#endif /* RTM_FRUEDITOR_H_ */