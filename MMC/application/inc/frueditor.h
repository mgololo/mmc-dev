/*! \file frueditor.c \ Creation of FRU info binary - Header file */
//*****************************************************************************
//
// File Name	: 'frueditor.c'
// Title		: Creation of FRU info binary - Header file
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************

#ifndef FRUEDITOR_H
#define FRUEDITOR_H

	#define MAX_AMC_PORT_NB	20
	
	typedef struct{
		unsigned char port;						/*AMC port*/
		unsigned char protocol;					/*Protocol*/
		unsigned char extension;					/*Protocol extension*/
		unsigned char OEM[16];			/*OEM id -> used for OEM protocol*/
		unsigned char oem_id;						/*Automatically generated*/
		unsigned char matching;					/*Matching*/
	}PortEkey_t;
	
	typedef struct{
		unsigned char reserved:6;
		unsigned char asymmetric_match:2;
		unsigned char link_group:8;
		unsigned char link_ext:4;
		unsigned char link_type:8;
		unsigned char link_lane:4;
		unsigned char ch_desc:8;
	}PortLnkDesc_t;
	
	typedef struct{
		unsigned char reserved:4;
		unsigned char lane_3:5;
		unsigned char lane_2:5;
		unsigned char lane_1:5;
		unsigned char lane_0:5;	
	}PortChDesc_t;
	
	typedef struct{
		unsigned char clock_id;
		unsigned char clock_ctrl;
		unsigned char indirect_clock_desc_cnt;
		unsigned char direct_clock_desc_cnt;
		
		struct{
			unsigned char pll;
			unsigned char type;
			unsigned char dependant_id;
		} indirect_clock_desc[4];		
		
		struct{
			unsigned char pll;
			unsigned char type;
			unsigned char family;
			unsigned char accuracy;
			unsigned int freq;
			unsigned int freq_min;
			unsigned int freq_max;
		} direct_clock_desc[4];		
	}ClockDesc_t;
	
	void fru_header_function(unsigned short board_info_size, unsigned short product_info_size);
	unsigned short board_info_area_function(unsigned short addr);
	unsigned short product_info_area_function(unsigned short addr);
	unsigned short module_current_record_function(unsigned short addr);
	unsigned short port_point_to_point_record_frunction(unsigned short addr);
	void write_fru_binary();
	
#endif