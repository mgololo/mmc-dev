/*
 * hotswap_sensor.h
 *
 * Created: 22/05/2017 11:25:51
 *  Author: jumendez
 */ 


#ifndef HOTSWAP_SENSOR_H_
#define HOTSWAP_SENSOR_H_

unsigned char update_hotswap_value();
unsigned char get_hotswap_sdr_byte(signed char *byte, unsigned char len, unsigned char sensor_id, unsigned char offset);
unsigned char init_hotswap_sdr(unsigned char sdr_id, unsigned char sensor_id, unsigned char entity_inst, unsigned char entity_id, unsigned char owner_id);
unsigned char set_hotswap_threshold(unsigned char sensor_id, unsigned char mask, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr);

#define HOTSWAP_SDR		/* Manadatory : Hotswap */									\
{																					\
	0x03,            /* record number, LSB  - filled by SDR_Init() */				\
	0x00,            /* record number, MSB  - filled by SDR_Init() */				\
	0x51,            /* IPMI protocol version */									\
	0x01,            /* record type: full sensor */									\
	0x37,            /* record length: remaining bytes -> SDR_Init */				\
	0x00,            /* i2c address, -> SDR_Init */									\
	0x00,            /* sensor owner LUN */											\
	HOT_SWAP_SENSOR, /* sensor number */											\
	0xc1,            /* entity id: AMC Module */									\
	0x00,            /* entity instance -> SDR_Init */								\
	0x03,            /* init: event generation + scanning enabled */				\
	0xc1,            /* capabilities: auto re-arm, */								\
	HOT_SWAP,        /* sensor type: HOT SWAP */									\
	0x6f,            /* sensor reading */											\
	0x07,            /* LSB assert event mask: 3 bit value */						\
	0x00,            /* MSB assert event mask */									\
	0x00,            /* LSB deassert event mask: 3 bit value */						\
	0x00,            /* MSB deassert event mask */									\
	0x07,            /* LSB: discrete reading mask */								\
	0x00,            /* MSB: discrete reading mask */								\
	0xc0,            /* sensor units 1 */											\
	0x00,            /* sensor units 2 */											\
	0x00,            /* sensor units 3 */											\
	0x00,            /* Linearization */											\
	0x00,            /* M */														\
	0x00,            /* M - Tolerance */											\
	0x00,            /* B */														\
	0x00,            /* B - Accuracy */												\
	0x00,            /* Sensor direction */											\
	0x00,            /* R-Exp , B-Exp */											\
	0x00,            /* Analogue characteristics flags */							\
	0x00,            /* Nominal reading */											\
	0x00,            /* Normal maximum */											\
	0x00,            /* Normal minimum */											\
	0x00,            /* Sensor Maximum reading */									\
	0x00,            /* Sensor Minimum reading */									\
	0x00,            /* Upper non-recoverable Threshold */							\
	0x00,            /* Upper critical Threshold */									\
	0x00,            /* Upper non critical Threshold */								\
	0x00,            /* Lower non-recoverable Threshold */							\
	0x00,            /* Lower critical Threshold */									\
	0x00,            /* Lower non-critical Threshold */								\
	0x00,            /* positive going Threshold hysteresis value */				\
	0x00,            /* negative going Threshold hysteresis value */				\
	0x00,            /* reserved */													\
	0x00,            /* reserved */													\
	0x00,            /* OEM reserved */												\
	0xcc,            /* 8 bit ASCII, number of bytes */								\
	'A', 'M', 'C', ' ', 'H', 'O', 'T', '_', 'S', 'W', 'A', 'P' /* sensor string */	\
}

#endif /* HOTSWAP_SENSOR_H_ */