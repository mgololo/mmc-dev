/*! \file sensor.h \ SDR for generic sensors - Header file */
//*****************************************************************************
//
// File Name	: 'sensor.h'
// Title		: SDR for generic sensors - Header file
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************

#ifndef GENERIC_SENSORS_H_
#define GENERIC_SENSORS_H_

#define HOT_SWAP_SENSOR     0
#define VOLT_12				1

#endif /* SENSORS_H_ */