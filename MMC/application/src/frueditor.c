/*! \file frueditor.c \ Creation of FRU information binary */
//*****************************************************************************
//
// File Name	: 'frueditor.c'
// Title		: Creation of fru info binary
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************
#include <stdlib.h>
#include <string.h>

#include "../../drivers/drivers.h"

#include "../../user/user_code/fru_info.h"

#include "../inc/fru.h"
#include "../inc/toolfunctions.h"
#include "../inc/frueditor.h"
#include "../inc/languages.h"

#ifndef MODULE_CURRENT_RECORD
	#error "At least one current record should be defined"
#endif

void fru_header_function(unsigned short board_info_size, unsigned short product_info_size){

	unsigned short addr = 0x00;
	unsigned char crc = 0x00;
	
	unsigned char next_offset = 0x01;	//Header
	
	write_eeprom_byte(addr++, 0x01);
	crc += 0x01;
	
	write_eeprom_byte(addr++, 0x00);	/*No internal use area for the MMC*/
	write_eeprom_byte(addr++, 0x00);	/*No chassis info area for the MMC*/

	write_eeprom_byte(addr++, next_offset);
	crc += next_offset;
	next_offset += (unsigned char)(board_info_size/8);
	
	write_eeprom_byte(addr++, next_offset);
	crc += next_offset;
	next_offset += (unsigned char)(product_info_size/8);

	write_eeprom_byte(addr++, next_offset);
	crc += next_offset;
	
	write_eeprom_byte(addr++, 0x00);
	write_eeprom_byte(addr++, (0 - crc));	
}


unsigned short board_info_area_function(unsigned short addr){

	int i;
	unsigned char crc = 0x00;
	unsigned short start_addr = addr;
	
	unsigned short length = 13 + strlen(BOARD_MANUFACTURER) +
		strlen(BOARD_NAME) +
		strlen(BOARD_SN) +
		strlen(BOARD_PN) +
		strlen(FRU_FILE_ID);
	
	while(length%8)
		length++;
		
	write_eeprom_byte(addr++, 0x01);
	crc += 0x01;
	
	write_eeprom_byte(addr++, (unsigned char)(length/8));
	crc += (unsigned char)(length/8);
	
	write_eeprom_byte(addr++, LANG_CODE);
	crc += LANG_CODE;
	
	write_eeprom_byte(addr++, 0x00);	
	write_eeprom_byte(addr++, 0x00);	
	write_eeprom_byte(addr++, 0x00);
	
	/*Write BOARD_MANUFACTURER information*/
	write_eeprom_byte(addr++, 0xC0 | (strlen(BOARD_MANUFACTURER) & 0x3F));
	crc += 0xC0 | (strlen(BOARD_MANUFACTURER) & 0x3F);
	
	for(i=0; i < strlen(BOARD_MANUFACTURER); i++){
		write_eeprom_byte(addr++, BOARD_MANUFACTURER[i]);
		crc += BOARD_MANUFACTURER[i];
	}
	
	/*Write BOARD_NAME information*/
	write_eeprom_byte(addr++, 0xC0 | (strlen(BOARD_NAME) & 0x3F));
	crc += 0xC0 | (strlen(BOARD_NAME) & 0x3F);
	
	for(i=0; i < strlen(BOARD_NAME); i++){
		write_eeprom_byte(addr++, BOARD_NAME[i]);
		crc += BOARD_NAME[i];
	}
	
	/*Write BOARD_SN information*/
	write_eeprom_byte(addr++, 0xC0 | (strlen(BOARD_SN) & 0x3F));
	crc += 0xC0 | (strlen(BOARD_SN) & 0x3F);
	
	for(i=0; i < strlen(BOARD_SN); i++){
		write_eeprom_byte(addr++, BOARD_SN[i]);
		crc += BOARD_SN[i];
	}
	
	/*Write BOARD_PN information*/
	write_eeprom_byte(addr++, 0xC0 | (strlen(BOARD_PN) & 0x3F));
	crc += 0xC0 | (strlen(BOARD_PN) & 0x3F);
	
	for(i=0; i < strlen(BOARD_PN); i++){
		write_eeprom_byte(addr++, BOARD_PN[i]);
		crc += BOARD_PN[i];
	}
	
	/*Write FRU_FILE_ID information */
	write_eeprom_byte(addr++, 0xC0 | (strlen(FRU_FILE_ID) & 0x3F));
	crc += 0xC0 | (strlen(FRU_FILE_ID) & 0x3F);
	
	for(i=0; i < strlen(FRU_FILE_ID); i++){
		write_eeprom_byte(addr++, FRU_FILE_ID[i]);
		crc += FRU_FILE_ID[i];
	}
	
	/*End of Board info area*/
	write_eeprom_byte(addr++, 0xC1);
	crc += 0xC1;
	
	/*Checksum - End_addr = start_addr+length*/
	//addr = start_addr+(length-1);	--Bug fix (Wrong checksum when EEPROM was not empty)
	
	/* Zero-Fill remaining bytes before checksum */
	for(i=addr; i < (start_addr + length-1); i++) {
		write_eeprom_byte(addr++, 0x00);
	}
	
	write_eeprom_byte(addr++, (0 - crc));
	
	return addr;
}

unsigned short product_info_area_function(unsigned short addr){

	int i;
	unsigned char crc = 0x00;
	unsigned short start_addr = addr;
	
	unsigned short length = 12 + strlen(PRODUCT_MANUFACTURER) +
		strlen(PRODUCT_NAME) +
		strlen(PRODUCT_PN) +
		strlen(PRODUCT_VERSION) +
		strlen(PRODUCT_SN) +
		strlen(PRODUCT_ASSET_TAG) +
		strlen(FRU_FILE_ID);
	
	while(length%8)
		length++;
	
	write_eeprom_byte(addr++, 0x01);
	crc += 0x01;
		
	write_eeprom_byte(addr++, (unsigned char)(length/8));
	crc += (unsigned char)(length/8);
	
	write_eeprom_byte(addr++, LANG_CODE);
	crc += LANG_CODE;
	
	/*Write PRODUCT_MANUFACTURER information*/
	write_eeprom_byte(addr++, 0xC0 | (strlen(PRODUCT_MANUFACTURER) & 0x3F));
	crc += 0xC0 | (strlen(PRODUCT_MANUFACTURER) & 0x3F);
	
	for(i=0; i < strlen(PRODUCT_MANUFACTURER); i++){
		write_eeprom_byte(addr++, PRODUCT_MANUFACTURER[i]);
		crc += PRODUCT_MANUFACTURER[i];
	}
	
	/*Write PRODUCT_NAME information*/
	write_eeprom_byte(addr++, 0xC0 | (strlen(PRODUCT_NAME) & 0x3F));
	crc += 0xC0 | (strlen(PRODUCT_NAME) & 0x3F);
	
	for(i=0; i < strlen(PRODUCT_NAME); i++){
		write_eeprom_byte(addr++, PRODUCT_NAME[i]);
		crc += PRODUCT_NAME[i];
	}
	
	/*Write PRODUCT_PN information*/
	write_eeprom_byte(addr++, 0xC0 | (strlen(PRODUCT_PN) & 0x3F));
	crc += 0xC0 | (strlen(PRODUCT_PN) & 0x3F);
	
	for(i=0; i < strlen(PRODUCT_PN); i++){
		write_eeprom_byte(addr++, PRODUCT_PN[i]);
		crc += PRODUCT_PN[i];
	}
	
	/*Write PRODUCT_VERSION information*/
	write_eeprom_byte(addr++, 0xC0 | (strlen(PRODUCT_VERSION) & 0x3F));
	crc += 0xC0 | (strlen(PRODUCT_VERSION) & 0x3F);
	
	for(i=0; i < strlen(PRODUCT_VERSION); i++){
		write_eeprom_byte(addr++, PRODUCT_VERSION[i]);
		crc += PRODUCT_VERSION[i];
	}
	
	/*Write PRODUCT_SN information*/
	write_eeprom_byte(addr++, 0xC0 | (strlen(PRODUCT_SN) & 0x3F));
	crc += 0xC0 | (strlen(PRODUCT_SN) & 0x3F);
	
	for(i=0; i < strlen(PRODUCT_SN); i++){
		write_eeprom_byte(addr++, PRODUCT_SN[i]);
		crc += PRODUCT_SN[i];
	}
	
	/*Write PRODUCT_ASSET_TAG information*/
	write_eeprom_byte(addr++, 0xC0 | (strlen(PRODUCT_ASSET_TAG) & 0x3F));
	crc += 0xC0 | (strlen(PRODUCT_ASSET_TAG) & 0x3F);
	
	for(i=0; i < strlen(PRODUCT_ASSET_TAG); i++){
		write_eeprom_byte(addr++, PRODUCT_ASSET_TAG[i]);
		crc += PRODUCT_ASSET_TAG[i];
	}
	
	/*Write FRU_FILE_ID information*/
	write_eeprom_byte(addr++, 0xC0 | (strlen(FRU_FILE_ID) & 0x3F));
	crc += 0xC0 | (strlen(FRU_FILE_ID) & 0x3F);
	
	for(i=0; i < strlen(FRU_FILE_ID); i++){
		write_eeprom_byte(addr++, FRU_FILE_ID[i]);
		crc += FRU_FILE_ID[i];
	}
	
	/*End of Board info area*/
	write_eeprom_byte(addr++, 0xC1);
	crc += 0xC1;
	
	/*Checksum - End_addr = start_addr+length*/
	//addr = start_addr+(length-1);	--Bug fix (Wrong checksum when EEPROM was not empty)
	
	/* Zero-Fill remaining bytes before checksum */
	for(i=addr; i < (start_addr + length-1); i++) {
		write_eeprom_byte(addr++, 0x00);
	}
	
	write_eeprom_byte(addr++, (0 - crc));
	
	return addr;
}

#define current_in_ma(curr)		(unsigned char)(curr/100)

unsigned short module_current_record_function(unsigned short addr){
	
	unsigned char header_crc = 0x00;
	unsigned char record_crc = 0x00;
	
	unsigned short header_crc_addr = 0x00;
	unsigned short record_crc_addr = 0x00;
	
	/*Record header*/
	write_eeprom_byte(addr++, 0xC0);
	header_crc += 0xC0;
	
	write_eeprom_byte(addr++, 0x82);
	header_crc += 0x82;
	
	write_eeprom_byte(addr++, 0x06);
	header_crc += 0x06;
	
	record_crc_addr = addr;
	write_eeprom_byte(addr++, 0x00);
	
	header_crc_addr = addr;
	write_eeprom_byte(addr++, 0x00);
	
	/*Record payload*/
	write_eeprom_byte(addr++, 0x5a);
	record_crc += 0x5a;
	
	write_eeprom_byte(addr++, 0x31);
	record_crc += 0x31;
	
	write_eeprom_byte(addr++, 0x00);
	record_crc += 0x00;
	
	write_eeprom_byte(addr++, 0x16);
	record_crc += 0x16;
	
	write_eeprom_byte(addr++, 0x00);
	record_crc += 0x00;
	
	write_eeprom_byte(addr++, MODULE_CURRENT_RECORD);
	record_crc += MODULE_CURRENT_RECORD;
	
	write_eeprom_byte(record_crc_addr, (0 - record_crc));
	header_crc += (0 - record_crc);
	
	write_eeprom_byte(header_crc_addr, (0 - header_crc));
	
	return addr;
}

unsigned short port_point_to_point_record_frunction(unsigned short addr){
	#ifdef PORTCONF_LIST
		unsigned char i, j;
		
		unsigned char check_if_exists = 0;
		unsigned char port;
		unsigned char chdesc_id = 0;
		unsigned char lnk_id = 0;
		unsigned char oem_id = 0;
		unsigned char *chdesc_ptr;
		unsigned char *lnkdesc_ptr;
		unsigned char record_length;
		
		unsigned char header_crc = 0x00;
		unsigned char record_crc = 0x00;
		
		unsigned short start_addr = addr;
		
		unsigned short header_crc_addr = 0x00;
		unsigned short record_crc_addr = 0x00;
		unsigned short record_length_addr = 0x00;
		unsigned short oem_cnt_addr = 0x00;
		unsigned short chdesc_cnt_addr = 0x00;
		
		PortEkey_t port_conf[] = PORTCONF_LIST;
		
		PortChDesc_t chdesc[sizeof(port_conf)/sizeof(PortEkey_t)];
		PortLnkDesc_t lnkdesc[sizeof(port_conf)/sizeof(PortEkey_t)];
		
		/*Write record header*/
		write_eeprom_byte(addr++, 0xC0);
		header_crc += 0xC0;
		
		write_eeprom_byte(addr++, 0x02);
		header_crc += 0x02;
		
		record_length_addr = addr;
		write_eeprom_byte(addr++, 0x00);	/*Length - Should be calculated at the end*/
		
		record_crc_addr = addr;
		write_eeprom_byte(addr++, 0x00);
		
		header_crc_addr = addr;
		write_eeprom_byte(addr++, 0x00);		
		
		/*Record payload*/
		write_eeprom_byte(addr++, 0x5a);
		record_crc += 0x5a;
		
		write_eeprom_byte(addr++, 0x31);
		record_crc += 0x31;
		
		write_eeprom_byte(addr++, 0x00);
		record_crc += 0x00;
		
		write_eeprom_byte(addr++, 0x19);
		record_crc += 0x19;
		
		write_eeprom_byte(addr++, 0x00);
		record_crc += 0x00;
		
		oem_cnt_addr = addr++;
		
		/*Write OEMs*/
		for(j=0; j < (sizeof(port_conf)/sizeof(PortEkey_t)); j++){
			if(port_conf[j].protocol == OEM_TYPE){
				/*Write OEM into FRU EEPROM*/
				for(i=0; i<16; i++){
					write_eeprom_byte(addr++, port_conf[j].OEM[i]);
					record_crc += port_conf[j].OEM[i];
				}
				port_conf[j].oem_id = oem_id;
				oem_id++;
			}
		}
		write_eeprom_byte(oem_cnt_addr, oem_id);
		record_crc += oem_id;
		
		/* Point to point record computing */
		for(i=0; i < MAX_AMC_PORT_NB; i++){
			
			check_if_exists=0;
			
			for(j=0; j < (sizeof(port_conf)/sizeof(PortEkey_t)); j++){
				if(port_conf[j].port == i){
					check_if_exists = 1;
					break;
				}
			}
			
			if(check_if_exists){
				chdesc[chdesc_id].reserved = 0x0F;
				chdesc[chdesc_id].lane_3 = 0x1F;
				chdesc[chdesc_id].lane_2 = 0x1F;
				chdesc[chdesc_id].lane_1 = 0x1F;
				chdesc[chdesc_id].lane_0 = i;
				
				for(j=0; j < (sizeof(port_conf)/sizeof(PortEkey_t)); j++){
					if(port_conf[j].port == i){
						//Configuration must be wrote
						lnkdesc[lnk_id].reserved = 0x3F;
						lnkdesc[lnk_id].asymmetric_match = port_conf[j].matching;
						lnkdesc[lnk_id].link_group = 0;
						if(port_conf[j].extension == OEM_TYPE){
							lnkdesc[lnk_id].link_type = port_conf[j].protocol + port_conf[j].oem_id;
							lnkdesc[lnk_id].link_ext = port_conf[j].extension;
							lnkdesc[lnk_id].link_lane = 0x01;
							lnkdesc[lnk_id].ch_desc = chdesc_id;
						}else{
							lnkdesc[lnk_id].link_ext = port_conf[j].extension;
							lnkdesc[lnk_id].link_type = port_conf[j].protocol;
							lnkdesc[lnk_id].link_lane = 0x01;
							lnkdesc[lnk_id].ch_desc = chdesc_id; 
						}
						
						lnk_id++;
					}
				}
				
				chdesc_id++;
			}
		}
		
		
		/* Point to point record writting */
		write_eeprom_byte(addr++, 0x80);
		record_crc += 0x80;
		
		write_eeprom_byte(addr++, chdesc_id);
		record_crc += chdesc_id;
		
		for(i=0; i < chdesc_id; i++){
						
			write_eeprom_byte(addr++, ((chdesc[i].lane_1 & 0x07) << 5) | (chdesc[i].lane_0 & 0x1f));
			record_crc += ((chdesc[i].lane_1 & 0x07) << 5) | (chdesc[i].lane_0 & 0x1f);
			
			write_eeprom_byte(addr++, ((chdesc[i].lane_3 & 0x01) << 7) | ((chdesc[i].lane_2 & 0x1f) << 2) | ((chdesc[i].lane_1 & 0x18) >> 3));
			record_crc += ((chdesc[i].lane_3 & 0x01) << 7) | ((chdesc[i].lane_2 & 0x1f) << 2) | ((chdesc[i].lane_1 & 0x18) >> 3);
			
			write_eeprom_byte(addr++, ((chdesc[i].reserved & 0x0F) << 4) | ((chdesc[i].lane_3 & 0x1e) >> 1));
			record_crc += ((chdesc[i].reserved & 0x0F) << 4) | ((chdesc[i].lane_3 & 0x1e) >> 1);
			
		}
		
		for(i=0; i < lnk_id; i++){
			
			write_eeprom_byte(addr++, lnkdesc[i].ch_desc);
			record_crc += lnkdesc[i].ch_desc;			
			
			write_eeprom_byte(addr++, ((lnkdesc[i].link_type & 0x0F) << 4) | (lnkdesc[i].link_lane & 0x0F));
			record_crc += ((lnkdesc[i].link_type & 0x0F) << 4) | (lnkdesc[i].link_lane & 0x0F);
			
			write_eeprom_byte(addr++, ((lnkdesc[i].link_ext & 0x0F) << 4) | ((lnkdesc[i].link_type & 0xF0) >> 4));
			record_crc += ((lnkdesc[i].link_ext & 0x0F) << 4) | ((lnkdesc[i].link_type & 0xF0) >> 4);
						
			write_eeprom_byte(addr++, lnkdesc[i].link_group);
			record_crc += lnkdesc[i].link_group;
			
			write_eeprom_byte(addr++, ((lnkdesc[i].reserved & 0x3F) << 2) | (lnkdesc[i].asymmetric_match & 0x03));
			record_crc += ((lnkdesc[i].reserved & 0x3F) << 2) | (lnkdesc[i].asymmetric_match & 0x03);				
		}
		
		record_length = (unsigned char)((addr-start_addr)-5);
		write_eeprom_byte(record_length_addr, record_length);
		header_crc += record_length;
		
		write_eeprom_byte(record_crc_addr, (0 - record_crc));
		header_crc += (0 - record_crc);
		
		write_eeprom_byte(header_crc_addr, (0 - header_crc));
	#endif
	
	return addr;
}

unsigned short clock_point_to_point_record_frunction(unsigned short addr){
	
	#ifdef CLOCK_LIST
	
		unsigned char i, j;
		
		unsigned char header_crc = 0x00;
		unsigned char record_crc = 0x00;
		unsigned short header_crc_addr = 0x00;
		unsigned short record_crc_addr = 0x00;
		unsigned short record_length_addr = 0x00;
	
		unsigned char record_length;
		unsigned short start_addr = addr;
		
		ClockDesc_t descriptor[] = CLOCK_LIST;
		
		/*Write record header*/
		write_eeprom_byte(addr++, 0xC0);
		header_crc += 0xC0;
		
		write_eeprom_byte(addr++, 0x02);
		header_crc += 0x02;
		
		record_length_addr = addr;
		write_eeprom_byte(addr++, 0x00);	/*Length - Should be calculated at the end*/
		
		record_crc_addr = addr;
		write_eeprom_byte(addr++, 0x00);
		
		header_crc_addr = addr;
		write_eeprom_byte(addr++, 0x00);
		
		/*Record payload*/
		write_eeprom_byte(addr++, 0x5a);
		record_crc += 0x5a;
		
		write_eeprom_byte(addr++, 0x31);
		record_crc += 0x31;
		
		write_eeprom_byte(addr++, 0x00);
		record_crc += 0x00;
		
		write_eeprom_byte(addr++, 0x2D);
		record_crc += 0x2D;
		
		write_eeprom_byte(addr++, 0x00);
		record_crc += 0x00;
		
		write_eeprom_byte(addr++, 0xFF);
		record_crc += 0xFF;
		
		write_eeprom_byte(addr++, sizeof(descriptor)/sizeof(ClockDesc_t));
		record_crc += sizeof(descriptor)/sizeof(ClockDesc_t);
		
		for(i=0; i < sizeof(descriptor)/sizeof(ClockDesc_t); i++){
			
			write_eeprom_byte(addr++, descriptor[i].clock_id);
			record_crc += descriptor[i].clock_id;
			
			write_eeprom_byte(addr++, descriptor[i].clock_ctrl);
			record_crc += descriptor[i].clock_ctrl;
			
			write_eeprom_byte(addr++, descriptor[i].indirect_clock_desc_cnt);
			record_crc += descriptor[i].indirect_clock_desc_cnt;
			
			write_eeprom_byte(addr++, descriptor[i].direct_clock_desc_cnt);
			record_crc += descriptor[i].direct_clock_desc_cnt;
			
			for(j=0; j < descriptor[i].indirect_clock_desc_cnt; j++){
				write_eeprom_byte(addr++, descriptor[i].indirect_clock_desc[j].pll | descriptor[i].indirect_clock_desc[j].type);
				record_crc += descriptor[i].indirect_clock_desc[j].pll | descriptor[i].indirect_clock_desc[j].type;
				
				write_eeprom_byte(addr++, descriptor[i].indirect_clock_desc[j].dependant_id);
				record_crc += descriptor[i].indirect_clock_desc[j].dependant_id;				
			}
		
			for(j=0; j < descriptor[i].direct_clock_desc_cnt; j++){
				write_eeprom_byte(addr++, descriptor[i].direct_clock_desc[j].pll | descriptor[i].direct_clock_desc[j].type);
				record_crc += descriptor[i].direct_clock_desc[j].pll | descriptor[i].direct_clock_desc[j].type;
				
				write_eeprom_byte(addr++, descriptor[i].direct_clock_desc[j].family);
				record_crc += descriptor[i].direct_clock_desc[j].family;
				
				write_eeprom_byte(addr++, descriptor[i].direct_clock_desc[j].accuracy);
				record_crc += descriptor[i].direct_clock_desc[j].accuracy;
				
				/* Frequence (LSB first)*/
				
				write_eeprom_byte(addr++, (unsigned char) (descriptor[i].direct_clock_desc[j].freq & 0x000000FF));
				record_crc += (unsigned char) (descriptor[i].direct_clock_desc[j].freq & 0x000000FF);
				
				write_eeprom_byte(addr++, (unsigned char) ((descriptor[i].direct_clock_desc[j].freq & 0x0000FF00) >> 8));
				record_crc += (unsigned char) ((descriptor[i].direct_clock_desc[j].freq & 0x0000FF00) >> 8);
				
				write_eeprom_byte(addr++, (unsigned char) ((descriptor[i].direct_clock_desc[j].freq & 0x00FF0000) >> 16));
				record_crc += (unsigned char) ((descriptor[i].direct_clock_desc[j].freq & 0x00FF0000) >> 16);
				
				write_eeprom_byte(addr++, (unsigned char) ((descriptor[i].direct_clock_desc[j].freq & 0xFF000000) >> 24));
				record_crc += (unsigned char) ((descriptor[i].direct_clock_desc[j].freq & 0xFF000000) >> 24);
				
				/* Min Frequence (LSB first)*/
				
				write_eeprom_byte(addr++, (unsigned char) (descriptor[i].direct_clock_desc[j].freq_min & 0x000000FF));
				record_crc += (unsigned char) (descriptor[i].direct_clock_desc[j].freq_min & 0x000000FF);
				
				write_eeprom_byte(addr++, (unsigned char) ((descriptor[i].direct_clock_desc[j].freq_min & 0x0000FF00) >> 8));
				record_crc += (unsigned char) ((descriptor[i].direct_clock_desc[j].freq_min & 0x0000FF00) >> 8);
				
				write_eeprom_byte(addr++, (unsigned char) ((descriptor[i].direct_clock_desc[j].freq_min & 0x00FF0000) >> 16));
				record_crc += (unsigned char) ((descriptor[i].direct_clock_desc[j].freq_min & 0x00FF0000) >> 16);
				
				write_eeprom_byte(addr++, (unsigned char) ((descriptor[i].direct_clock_desc[j].freq_min & 0xFF000000) >> 24));
				record_crc += (unsigned char) ((descriptor[i].direct_clock_desc[j].freq_min & 0xFF000000) >> 24);
				
				/* Max Frequence (LSB first)*/
				
				write_eeprom_byte(addr++, (unsigned char) (descriptor[i].direct_clock_desc[j].freq_max & 0x000000FF));
				record_crc += (unsigned char) (descriptor[i].direct_clock_desc[j].freq_max & 0x000000FF);
				
				write_eeprom_byte(addr++, (unsigned char) ((descriptor[i].direct_clock_desc[j].freq_max & 0x0000FF00) >> 8));
				record_crc += (unsigned char) ((descriptor[i].direct_clock_desc[j].freq_max & 0x0000FF00) >> 8);
				
				write_eeprom_byte(addr++, (unsigned char) ((descriptor[i].direct_clock_desc[j].freq_max & 0x00FF0000) >> 16));
				record_crc += (unsigned char) ((descriptor[i].direct_clock_desc[j].freq_max & 0x00FF0000) >> 16);
				
				write_eeprom_byte(addr++, (unsigned char) ((descriptor[i].direct_clock_desc[j].freq_max & 0xFF000000) >> 24));
				record_crc += (unsigned char) ((descriptor[i].direct_clock_desc[j].freq_max & 0xFF000000) >> 24);
			}
			
		}
	
		record_length = (unsigned char)((addr-start_addr)-5);
		write_eeprom_byte(record_length_addr, record_length);
		header_crc += record_length;
		
		write_eeprom_byte(record_crc_addr, (0 - record_crc));
		header_crc += (0 - record_crc);
		
		write_eeprom_byte(header_crc_addr, (0 - header_crc));
		
	#endif
	
	return addr;
}

void write_fru_binary(){
	unsigned short addr = 8;
	unsigned short tmp = 0;
	
	unsigned short board_info_area_size = 0;
	unsigned short product_info_area_size = 0;
	
	tmp = board_info_area_function(addr);
	board_info_area_size = tmp-addr;
	addr = tmp;
	
	tmp = product_info_area_function(addr);
	product_info_area_size = tmp-addr;
	addr = tmp;
	
	addr = port_point_to_point_record_frunction(addr);
	addr = clock_point_to_point_record_frunction(addr);
	addr = module_current_record_function(addr);
	
	fru_header_function(board_info_area_size, product_info_area_size);
}