/*
 * _12volts_sensor.c
 *
 * Created: 22/05/2017 11:50:43
 *  Author: jumendez
 */ 

#include "../../drivers/drivers.h"

#include "../inc/ipmi_if.h"
#include "../inc/sdr.h"
#include "../inc/sensors.h"
#include "../inc/12volts_sensor.h"

signed char amc_12v_sdr[] = V12_SDR;

unsigned char update_volt12_value(){
	set_sensor_value(VOLT_12, payload_voltage_sensor());
}

unsigned int sizesdr;

unsigned char get_volt12_sdr_byte(signed char *byte, unsigned char len, unsigned char sensor_id, unsigned char offset){
	unsigned int i, j;
	unsigned char read_len = 0;
	
	if(sensor_id != VOLT_12) return 0x00;
	
	for(j=0; j < len && (j+offset) < sizeof(amc_12v_sdr); j++, read_len++){
		byte[j] = amc_12v_sdr[j+offset];
	}
	
	return read_len;
}

unsigned char init_volt12_sdr(unsigned char sdr_id, unsigned char sensor_id, unsigned char entity_inst,  unsigned char entity_id, unsigned char owner_id){
		
	amc_12v_sdr[0] = 0x02;							//Record ID [LSB] (0x02)
	amc_12v_sdr[1] = 0x00;							//Record ID [MSB] (0x00)
	
	amc_12v_sdr[4] = sizeof(amc_12v_sdr) - 5;		//Record length (sizeof(sdr) - sizeof(header)) where header size is 5 bytes
	
	sizesdr = sizeof(amc_12v_sdr);
	
	amc_12v_sdr[5] = owner_id;						//Sensor owner ID
	amc_12v_sdr[6] = 0x70;							//Sensor owner LUN (only LUN 0x00, located on IPMB-L bus (0x07) is accepted by the CERN MMC)
	
	amc_12v_sdr[7] = VOLT_12;						//Sensor number
	
	amc_12v_sdr[8] = entity_id;						//Entity ID
	amc_12v_sdr[9] = entity_inst;					//Entity instance
}

unsigned char set_volt12_threshold(unsigned char sensor_id, unsigned char mask, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr){
	//Not supported yet
}