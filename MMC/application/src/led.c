//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
// File Name	: led.c
// 
// Title		: LED control file
// Revision		: 1.1
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
// Modified by  : Frederic Bompard CPPM ( Centre Physique des Particules de Marseille )
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
// Description : Control LEDs on front panel
//					
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************

// Header file
#include <string.h>

#include "../../drivers/drivers.h"

#include "../../user/user_code/config.h"

#include "../inc/rtm_mng.h"
#include "../inc/i2crtm.h"
#include "../inc/project.h"
#include "../inc/led.h"

#undef NUM_OF_AMC_LED
	
#ifdef AMC_USER_LED_LIST
	#define NUM_OF_AMC_LED	3+AMC_USER_LED_CNT
#else
	#define NUM_OF_AMC_LED	3
#endif
	
#ifdef USE_RTM
	#undef NUM_OF_RTM_LED
		
	#ifdef RTM_USER_LED_LIST
		#define NUM_OF_RTM_LED	3+RTM_USER_LED_CNT
	#else
		#define NUM_OF_RTM_LED	3
	#endif
#endif

//Globals
leds amc_led[NUM_OF_AMC_LED];

#ifdef USE_RTM
	leds rtm_led[NUM_OF_RTM_LED];
#endif

//*************/
void leds_init()    //Called from mmc_main.c
//*************/
{
	unsigned char i;
		
	LED_t amc_generic_led[3] = AMC_GENERIC_LED_LIST;
	LED_t led_amc[NUM_OF_AMC_LED];
		
	#ifdef AMC_USER_LED_LIST
		LED_t amc_user_led[AMC_USER_LED_CNT] = AMC_USER_LED_LIST;
	#endif
		
	#ifdef USE_RTM	
		LED_t rtm_generic_led[] = RTM_GENERIC_LED_LIST;
		#ifdef RTM_USER_LED_LIST
			LED_t rtm_user_led[] = RTM_USER_LED_LIST;
		#endif
	#endif
		
	memcpy(led_amc, amc_generic_led, 3*sizeof(LED_t));
		
	#ifdef AMC_USER_LED_LIST
		if(AMC_USER_LED_CNT > 0)
			memcpy(&led_amc[3], amc_user_led, AMC_USER_LED_CNT*sizeof(LED_t));
	#endif
		
	for(i=0; i < NUM_OF_AMC_LED; i++){
		amc_led[i].local_cntr_fnc  = led_amc[i].init;
		amc_led[i].fnc_off         = led_amc[i].init;
		amc_led[i].on_duration     = 0;
		amc_led[i].colour           = led_amc[i].colour;
		amc_led[i].control_state   = LOCAL_CONTROL_STATE;
		local_led_control(AMC, i, led_amc[i].init);
	}
		
	#ifdef USE_RTM
		for(i=0; i < NUM_OF_RTM_LED; i++){
			if(i<3){
				rtm_led[i].local_cntr_fnc  = rtm_generic_led[i].init;
				rtm_led[i].fnc_off         = rtm_generic_led[i].init;
				rtm_led[i].on_duration     = 0;
				rtm_led[i].colour           = rtm_generic_led[i].colour;
				rtm_led[i].control_state   = LOCAL_CONTROL_STATE;
			}else{
				rtm_led[i].local_cntr_fnc  = rtm_user_led[i].init;
				rtm_led[i].fnc_off         = rtm_user_led[i].init;
				rtm_led[i].on_duration     = 0;
				rtm_led[i].colour           = rtm_user_led[i].colour;
				rtm_led[i].control_state   = LOCAL_CONTROL_STATE;				
			}
			//local_led_control(RTM, i, led_rtm[i].init);
		}
	#endif
	
	timer_10ms_attach(ledTimerCallback);
}

unsigned char get_num_of_led(unsigned char fru){
	if(fru == AMC)	return NUM_OF_AMC_LED;
	#ifdef USE_RTM
		else if(fru == RTM)		return NUM_OF_RTM_LED;
	#endif
	else
		return 0x00;
}
void reset_led(unsigned char fru, unsigned char led_n){
	
	//unsigned char data;
	LED_t generic_led[3] = AMC_GENERIC_LED_LIST;
	LED_t amc_led_t[NUM_OF_AMC_LED];
	
	#ifdef AMC_USER_LED_LIST
		LED_t user_led[AMC_USER_LED_CNT] = AMC_USER_LED_LIST;
	#endif
	
	#ifdef USE_RTM
		LED_t rtm_generic_led[3] = RTM_GENERIC_LED_LIST;
		LED_t rtm_led_t[NUM_OF_RTM_LED];
		
		#ifdef RTM_USER_LED_LIST
			LED_t rtm_user_led[RTM_USER_LED_CNT] = RTM_USER_LED_LIST;
			if(RTM_USER_LED_CNT > 0)
				memcpy(&rtm_user_led[3], rtm_user_led, RTM_USER_LED_CNT*sizeof(LED_t));
		#endif
		
		memcpy(rtm_led_t, rtm_generic_led, 3*sizeof(LED_t));
	#endif
	
	#ifdef AMC_USER_LED_LIST
		if(AMC_USER_LED_CNT > 0)
			memcpy(&amc_led_t[3], user_led, AMC_USER_LED_CNT*sizeof(LED_t));
	#endif
	
	memcpy(amc_led_t, generic_led, 3*sizeof(LED_t));
	
	if(fru == AMC){
		amc_led[led_n].control_state = LOCAL_CONTROL_STATE; // initial state
		local_led_control(AMC, led_n, amc_led_t[led_n].init);
	}
	#ifdef USE_RTM
		else if(fru == RTM){
			rtm_led[led_n].control_state = LOCAL_CONTROL_STATE; // initial state
			local_led_control(RTM, led_n, rtm_led_t[led_n].init);
		}
	#endif
}

unsigned char state_led(unsigned char fru, unsigned char led_n){
	unsigned char data;
	LED_t generic_led[3] = AMC_GENERIC_LED_LIST;
	LED_t amc_led_t[NUM_OF_AMC_LED];
		
	#ifdef AMC_USER_LED_LIST
		LED_t user_led[AMC_USER_LED_CNT] = AMC_USER_LED_LIST;
	#endif
		
	#ifdef USE_RTM
		LED_t rtm_generic_led[3] = RTM_GENERIC_LED_LIST;
		LED_t rtm_led_t[NUM_OF_RTM_LED];
			
		#ifdef RTM_USER_LED_LIST
			LED_t rtm_user_led[RTM_USER_LED_CNT] = RTM_USER_LED_LIST;
			if(RTM_USER_LED_CNT > 0)
				memcpy(&rtm_user_led[3], rtm_user_led, RTM_USER_LED_CNT*sizeof(LED_t));
		#endif
			
		memcpy(rtm_led_t, rtm_generic_led, 3*sizeof(LED_t));
	#endif
		
	#ifdef AMC_USER_LED_LIST
		if(AMC_USER_LED_CNT > 0)
			memcpy(&amc_led_t[3], user_led, AMC_USER_LED_CNT*sizeof(LED_t));
	#endif
		
	memcpy(amc_led_t, generic_led, 3*sizeof(LED_t));		
		
	if(fru == AMC && led_n >= NUM_OF_AMC_LED)		return 0xFF;
	#ifdef USE_RTM
		else if(fru == RTM && led_n >= NUM_OF_RTM_LED)	return 0xFF;
	#endif
		
	if(fru == AMC){
		switch(amc_led_t[led_n].io_type){
			case MMC_PORT:
				if(get_mmcport_led_state(&amc_led_t[led_n]) == amc_led_t[led_n].active)		return LED_ON;
				else																		return LED_OFF;
				break;
				
			case IO_EXTENDER_PCF8574AT:
				data = get_PCF8574_bit(amc_led_t[led_n].addr, amc_led_t[led_n].pin);					
				if((data?HIGH:LOW) == amc_led_t[led_n].active)					return LED_ON;
				else															return LED_OFF;
				break;
		}
	}
	#ifdef USE_RTM
		else if(fru == RTM){
			switch(rtm_led_t[led_n].io_type){
				case MMC_PORT:
					if(get_mmcport_led_state(&rtm_led_t[led_n]) == rtm_led_t[led_n].active)		return LED_ON;
					else																		return LED_OFF;
					break;
					
				case IO_EXTENDER_PCF8574AT:
					data = get_PCF8574_bit(rtm_led_t[led_n].addr, rtm_led_t[led_n].pin);
					if((data?HIGH:LOW) == rtm_led_t[led_n].active)					return LED_ON;
					else															return LED_OFF;
					break;
			}
		}
	#endif
	else
		return 0xff;	
	
    return (0xff); //MJ: mmc_main.c does not process this error. How could it react?
}

unsigned char tmp_var;

void local_led_control(unsigned char fru, unsigned char led_n, unsigned char led_state){
	//unsigned char data;
	LED_t generic_led[3] = AMC_GENERIC_LED_LIST;
	LED_t amc_led_t[NUM_OF_AMC_LED];
	LED_t *selected_led;
		
	#ifdef AMC_USER_LED_LIST
		LED_t user_led[AMC_USER_LED_CNT] = AMC_USER_LED_LIST;
	#endif
	
	#ifdef USE_RTM
		LED_t rtm_generic_led[] = RTM_GENERIC_LED_LIST;
		#ifdef RTM_USER_LED_LIST
			LED_t rtm_user_led[] = RTM_USER_LED_LIST;
		#endif
	#endif
		
	#ifdef AMC_USER_LED_LIST
		if(AMC_USER_LED_CNT > 0)
			memcpy(&amc_led_t[3], user_led, AMC_USER_LED_CNT*sizeof(LED_t));
	#endif
		
	memcpy(amc_led_t, generic_led, 3*sizeof(LED_t));
		
	
	if (amc_led[led_n].control_state == OVERRIDE_STATE || amc_led[led_n].control_state == LAMP_TEST_STATE)
		return;
	
	if(fru == AMC){
		switch(amc_led_t[led_n].io_type){
			case MMC_PORT:
				if(led_state == LED_ON)			active_mmcport_led(&amc_led_t[led_n]);
				else if(led_state == LED_OFF)	desactive_mmcport_led(&amc_led_t[led_n]);
				break;
			
			case IO_EXTENDER_PCF8574AT:
				if(led_state == LED_ON){						
					if(amc_led_t[led_n].active)			set_PCF8574_bit(amc_led_t[led_n].addr, amc_led_t[led_n].pin);
					else								clear_PCF8574_bit(amc_led_t[led_n].addr, amc_led_t[led_n].pin);
				}
				else if(led_state == LED_OFF){
					if(amc_led_t[led_n].inactive)		set_PCF8574_bit(amc_led_t[led_n].addr, amc_led_t[led_n].pin);
					else								clear_PCF8574_bit(amc_led_t[led_n].addr, amc_led_t[led_n].pin);
				}			
			break;
		}
		
		amc_led[led_n].local_cntr_fnc = led_state;
		amc_led[led_n].state = led_state;
	}
	#ifdef USE_RTM
		else if(fru == RTM){
			if(led_n < 3)	selected_led = &rtm_generic_led[led_n];
			else			selected_led = &rtm_user_led[led_n-3];
			
			switch(selected_led->io_type){
				case MMC_PORT:
					if(led_state == LED_ON)			active_mmcport_led(selected_led);
					else if(led_state == LED_OFF)	desactive_mmcport_led(selected_led);
					break;
					
				case IO_EXTENDER_PCF8574AT:
					
					if(led_state == LED_ON){
						if(selected_led->active)			set_PCF8574_bit(selected_led->addr, selected_led->pin);
						else								clear_PCF8574_bit(selected_led->addr, selected_led->pin);
					}
					else if(led_state == LED_OFF){
						if(selected_led->inactive)			set_PCF8574_bit(selected_led->addr, selected_led->pin);
						else								clear_PCF8574_bit(selected_led->addr, selected_led->pin);
					}				
					tmp_var= get_PCF8574_port(selected_led->addr);	
				break;
			}
		}
	#endif
}


void led_control(unsigned char fru, unsigned char led_n, unsigned char led_state){
	//unsigned char data;
	LED_t generic_led[3] = AMC_GENERIC_LED_LIST;
	LED_t amc_led_t[NUM_OF_AMC_LED];
	LED_t *selected_led;
	
	#ifdef AMC_USER_LED_LIST
		LED_t user_led[AMC_USER_LED_CNT] = AMC_USER_LED_LIST;
	#endif
		
	#ifdef USE_RTM
		LED_t rtm_generic_led[] = RTM_GENERIC_LED_LIST;
		#ifdef RTM_USER_LED_LIST
			LED_t rtm_user_led[] = RTM_USER_LED_LIST;
		#endif
	#endif
	
	#ifdef AMC_USER_LED_LIST
		if(AMC_USER_LED_CNT > 0)
		memcpy(&amc_led_t[3], user_led, AMC_USER_LED_CNT*sizeof(LED_t));
	#endif
	
	memcpy(amc_led_t, generic_led, 3*sizeof(LED_t));
	
	if(fru == AMC){
		
		if (amc_led[led_n].control_state == LOCAL_CONTROL_STATE)
			return;
		
		switch(amc_led_t[led_n].io_type){
			case MMC_PORT:
			if(led_state == LED_ON)			active_mmcport_led(&amc_led_t[led_n]);
			else if(led_state == LED_OFF)	desactive_mmcport_led(&amc_led_t[led_n]);
			break;
			
			case IO_EXTENDER_PCF8574AT:
			if(led_state == LED_ON){
				if(amc_led_t[led_n].active)			set_PCF8574_bit(amc_led_t[led_n].addr, amc_led_t[led_n].pin);
				else								clear_PCF8574_bit(amc_led_t[led_n].addr, amc_led_t[led_n].pin);
			}
			else if(led_state == LED_OFF){
				if(amc_led_t[led_n].inactive)		set_PCF8574_bit(amc_led_t[led_n].addr, amc_led_t[led_n].pin);
				else								clear_PCF8574_bit(amc_led_t[led_n].addr, amc_led_t[led_n].pin);
			}
			break;
		}
		
		amc_led[led_n].state = led_state;
	}
	#ifdef USE_RTM
		else if(fru == RTM){
			if(led_n < 3)	selected_led = &rtm_generic_led[led_n];
			else			selected_led = &rtm_user_led[led_n-3];
		
			switch(selected_led->io_type){
				case MMC_PORT:
				if(led_state == LED_ON)			active_mmcport_led(selected_led);
				else if(led_state == LED_OFF)	desactive_mmcport_led(selected_led);
				break;
			
				case IO_EXTENDER_PCF8574AT:
			
				if(led_state == LED_ON){
					if(selected_led->active)			set_PCF8574_bit(selected_led->addr, selected_led->pin);
					else								clear_PCF8574_bit(selected_led->addr, selected_led->pin);
				}
				else if(led_state == LED_OFF){
					if(selected_led->inactive)			set_PCF8574_bit(selected_led->addr, selected_led->pin);
					else								clear_PCF8574_bit(selected_led->addr, selected_led->pin);
				}
				tmp_var= get_PCF8574_port(selected_led->addr);
				break;
			}
		}
	#endif
}

unsigned char nb_of_led;

//*****************************************************************/
unsigned char ipmi_set_fru_led_state(unsigned char fru, unsigned char LedId, unsigned char LedFn, unsigned char LedOn)   //Called from ipmi_if.c
//*****************************************************************/
{
    if (fru == AMC)
    {		
        if (LedId >= NUM_OF_AMC_LED)                       // value out of range
            return (0xff);

		if (amc_led[LedId].control_state == LAMP_TEST_STATE)	//JM : Set FRU Led State should return completion code 0xCC if Lamp Test is in progress
		{
				return 0xff;
		}
        else if (LedFn >= 1 && LedFn <= 0xfa)	//JM : The Led blink function is not implemented => This command can not be executed
        {
            amc_led[LedId].fnc_off       = LedFn;          // ON/OFF/Off time
            amc_led[LedId].on_duration   = LedOn;          // ON time
            amc_led[LedId].control_state = OVERRIDE_STATE; // initial state
			amc_led[LedId].cnt			 = LedOn;
			led_control(AMC, LedId, LED_ON);			
        }
        else if (LedFn == LED_OFF || LedFn == LED_ON)
        {
            amc_led[LedId].fnc_off       = LedFn;            // ON/OFF/Off time
            amc_led[LedId].on_duration   = 0x00;             // ON time
            amc_led[LedId].control_state = OVERRIDE_STATE;   // initial state
            led_control(AMC, LedId, LedFn);
        }
        else if (LedFn == RESTORE_LOCAL_CONTROL)
        {
            amc_led[LedId].control_state = LOCAL_CONTROL_STATE; // initial state			
            local_led_control(AMC, LedId, amc_led[LedId].local_cntr_fnc);
        }
		else if (LedFn == LAMP_TEST_CMD){		//JM : Lamp Test case		
					
			if (LedOn >= 128){	//Test time value should be less than 128
				return (0xff);
			}
			
			//	Save the old state
			if (amc_led[LedId].control_state == LOCAL_CONTROL_STATE)
				amc_led[LedId].lamp_test_return_control_state = RESTORE_LOCAL_CONTROL;
			else
				amc_led[LedId].lamp_test_return_control_state = amc_led[LedId].fnc_off;
			
			//amc_led[LedId].fnc_off = LAMP_TEST_STATE;
			
			//	Set the control state
			amc_led[LedId].control_state = LAMP_TEST_STATE;
			
			//	Set cnt and duration : it is used to get the test lamp remaining time
			
			amc_led[LedId].lamp_test_duration = LedOn;
			amc_led[LedId].lamp_test_cnt = 0;
			led_control(AMC, LedId, LED_ON);
		}
        else
            return (0xff);
    }
	#ifdef USE_RTM
		else if (fru == RTM)
		{
			if (LedId >= NUM_OF_RTM_LED)                       // value out of range
				return (0xff);

			if (rtm_led[LedId].control_state == LAMP_TEST_STATE)	//JM : Set FRU Led State should return completion code 0xCC if Lamp Test is in progress
			{
				return 0xff;
			}
			else if (LedFn >= 1 && LedFn <= 0xfa)
			{
				rtm_led[LedId].fnc_off       = LedFn;          // ON/OFF/Off time
				rtm_led[LedId].on_duration   = LedOn;          // ON time
				rtm_led[LedId].control_state = OVERRIDE_STATE; // initial state
				rtm_led[LedId].cnt			 = LedOn;
				led_control(RTM, LedId, LED_ON);
			}
			else if (LedFn == LED_OFF || LedFn == LED_ON)
			{
				rtm_led[LedId].fnc_off       = LedFn;            // ON/OFF/Off time
				rtm_led[LedId].on_duration   = 0x00;             // ON time
				rtm_led[LedId].control_state = OVERRIDE_STATE;   // initial state
				led_control(RTM, LedId, LedFn);
			}
			else if (LedFn == RESTORE_LOCAL_CONTROL)
			{
				rtm_led[LedId].control_state = LOCAL_CONTROL_STATE; // initial state
				local_led_control(RTM, LedId, rtm_led[LedId].local_cntr_fnc);
			}
			else if (LedFn == LAMP_TEST_CMD){		//JM : Lamp Test case
			
				if (LedOn >= 128){	//Test time value should be less than 128
					return (0xff);
				}
			
				//	Save the old state
				if (rtm_led[LedId].control_state == LOCAL_CONTROL_STATE)
					rtm_led[LedId].lamp_test_return_control_state = RESTORE_LOCAL_CONTROL;
				else
					rtm_led[LedId].lamp_test_return_control_state = rtm_led[LedId].fnc_off;
			
				//amc_led[LedId].fnc_off = LAMP_TEST_STATE;
			
				//	Set the control state
				rtm_led[LedId].control_state = LAMP_TEST_STATE;
			
				//	Set cnt and duration : it is used to get the test lamp remaining time
			
				rtm_led[LedId].lamp_test_duration = LedOn;
				rtm_led[LedId].lamp_test_cnt = 0;
				led_control(RTM, LedId, LED_ON);
			}
			else
				return (0xff);
		}
	#endif
	else
		return 0xff;
	
    return (0);
}


//*****************************************************/
unsigned char ipmi_get_fru_led_state(unsigned char fru, unsigned char LedId, unsigned char *buf)   //Called from ipmi_if.c
//*****************************************************/
{
    unsigned char len = 0;

    if (fru == AMC)
    {
        if (LedId >= NUM_OF_AMC_LED)                 // value out of range
            return (0xff);

        buf[len++] = amc_led[LedId].control_state;   // LED States - override state has been enabled
        buf[len++] = amc_led[LedId].local_cntr_fnc;  // Local Control Function - not supported
        buf[len++] = amc_led[LedId].on_duration;     // Local control ON duration
        buf[len++] = amc_led[LedId].colour;           // Local colour: Blue or Red  //MJ: Where is green?
        if (amc_led[LedId].control_state == OVERRIDE_STATE || amc_led[LedId].control_state == LAMP_TEST_STATE)	//Send information in case of lamp test
        {
            buf[len++] = amc_led[LedId].fnc_off;     // Override state function - ON/OFF/Off time
            buf[len++] = amc_led[LedId].on_duration; // Override control ON duration
            buf[len++] = amc_led[LedId].colour;       // Override colour: Blue or Red
			
			if(amc_led[LedId].control_state == LAMP_TEST_STATE)	//Lamp test case : lamp test duration should be sent
				buf[len++] = amc_led[LedId].lamp_test_duration;
        }
    }
	#ifdef USE_RTM
		else if(fru == RTM){		
			if (LedId >= NUM_OF_RTM_LED)                 // value out of range
				return (0xff);

			buf[len++] = rtm_led[LedId].control_state;   // LED States - override state has been enabled
			buf[len++] = rtm_led[LedId].local_cntr_fnc;  // Local Control Function - not supported
			buf[len++] = rtm_led[LedId].on_duration;     // Local control ON duration
			buf[len++] = rtm_led[LedId].colour;           // Local colour: Blue or Red  //MJ: Where is green?
			if (rtm_led[LedId].control_state == OVERRIDE_STATE || rtm_led[LedId].control_state == LAMP_TEST_STATE)	//Send information in case of lamp test
			{
				buf[len++] = rtm_led[LedId].fnc_off;     // Override state function - ON/OFF/Off time
				buf[len++] = rtm_led[LedId].on_duration; // Override control ON duration
				buf[len++] = rtm_led[LedId].colour;       // Override colour: Blue or Red
			
				if(rtm_led[LedId].control_state == LAMP_TEST_STATE)	//Lamp test case : lamp test duration should be sent
					buf[len++] = rtm_led[LedId].lamp_test_duration;
			}
		}
	#endif
	else
		return 0xff;
		
    return (len);
}


//***********************************************/
unsigned char ipmi_get_fru_led_properties(unsigned char fru, unsigned char *buf)  //Called from ipmi_if.c
//***********************************************/
{
    unsigned char len = 0;

    buf[len++] = 0x07;           // support the control of 3 standard LEDs (BLUE LED, LED1, LED2). LED3 is not supported

    if(fru == 0)
        buf[len++] = NUM_OF_AMC_LED; // number of AMC LEDs under control
	#ifdef USE_RTM
		else if(fru == 1)
			buf[len++] = NUM_OF_RTM_LED; // number of RTM LEDs under control
	#endif
    else
        buf[len++] = 0; // Just in case....


    return (len);
}


//**************************************************************/
unsigned char ipmi_get_led_color_capabilities(unsigned char fru, unsigned char LedId, unsigned char *buf)   //Called from ipmmi_if.c
//**************************************************************/
{
    unsigned char len = 0;
	
    if (fru == AMC && LedId >= NUM_OF_AMC_LED)		// value out of range
        return(0xff);
	#ifdef USE_RTM
		else if (fru == RTM && LedId >= NUM_OF_RTM_LED)     // value out of range
			return(0xff);
	#endif
	
	if(fru == AMC){
		buf[len++] = 1 << amc_led[LedId].colour;  // capability
		buf[len++] = amc_led[LedId].colour;       // default colour
		buf[len++] = amc_led[LedId].colour;       // default colour in override state
	}else if(fru == RTM){
		#ifdef USE_RTM
			buf[len++] = 1 << rtm_led[LedId].colour;  // capability
			buf[len++] = rtm_led[LedId].colour;       // default colour
			buf[len++] = rtm_led[LedId].colour;       // default colour in override state				
		#else
			return 0xff;
		#endif
	}else
		return 0xff;
	
    return (len);
}

void ledTimerCallback(){	//Called every 10 ms
	unsigned short i;
	
	for (i=0; i<NUM_OF_AMC_LED; i++){
		if(amc_led[i].control_state == LAMP_TEST_STATE){
			amc_led[i].lamp_test_cnt++;
			
			if(amc_led[i].lamp_test_cnt >= (amc_led[i].lamp_test_duration*10)){	//Lamp test finished : lamp_test_duration is set in hundreds of milliseconds					
				amc_led[i].control_state = EMPTY;
				ipmi_set_fru_led_state(0, i, amc_led[i].lamp_test_return_control_state, amc_led[i].on_duration);	
			}
		}
		else if(amc_led[i].fnc_off >= 1 && amc_led[i].fnc_off <= 0xfa){
			amc_led[i].cnt--;
			
			if(--amc_led[i].cnt <= 0){
				switch(amc_led[i].state){
					case LED_ON:	amc_led[i].cnt = amc_led[i].fnc_off;
									led_control(AMC, i, LED_OFF);
									break;
					case LED_OFF:	amc_led[i].cnt = amc_led[i].on_duration;
									led_control(AMC, i, LED_ON);
									break;
				}
			}
		}
	}
	
	#ifdef USE_RTM
		for (i=0; i<NUM_OF_RTM_LED; i++){
			if(rtm_led[i].control_state == LAMP_TEST_STATE){
				rtm_led[i].lamp_test_cnt++;
			
				if(rtm_led[i].lamp_test_cnt >= (rtm_led[i].lamp_test_duration*10)){	//Lamp test finished : lamp_test_duration is set in hundreds of milliseconds
					rtm_led[i].control_state = EMPTY;
					ipmi_set_fru_led_state(RTM, i, rtm_led[i].lamp_test_return_control_state, rtm_led[i].on_duration);
				}
			}
			else if(rtm_led[i].fnc_off >= 1 && rtm_led[i].fnc_off <= 0xfa){
				rtm_led[i].cnt--;
			
				if(--rtm_led[i].cnt <= 0){
					switch(rtm_led[i].state){
						case LED_ON:	rtm_led[i].cnt = rtm_led[i].fnc_off;
										led_control(RTM, i, LED_OFF);
										break;
						case LED_OFF:	rtm_led[i].cnt = rtm_led[i].on_duration;
										led_control(RTM, i, LED_ON);
										break;
					}
				}
			}
		}
	#endif
}

/* Pin control functions */
unsigned char get_mmcport_led_state(LED_t *led){
	return get_signal(led->pin);	
	return 0x00;
}

void active_mmcport_led(LED_t *led){
	if(led->active){
		set_signal(led->pin);
	}
	else{
		clear_signal(led->pin);
	}
}

void desactive_mmcport_led(LED_t *led){
	if(led->inactive){
		set_signal(led->pin);
	}
	else{
		clear_signal(led->pin);
	}
}

