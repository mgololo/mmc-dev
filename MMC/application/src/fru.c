//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
// File Name	: fru.c
// 
// Title		: FRU device
// Revision		: 1.1
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
// Description : FRU(Field Replaceable Unit) information and payload control.
//					
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************
#include "../../drivers/drivers.h"

#include "../../user/user_code/sensors.h"
#include "../../user/user_code/config.h"
#include "../../user/user_code/fru_info.h"

#include "../inc/project.h"
#include "../inc/frueditor.h"				//FRU Editor related functions
#include "../inc/i2crtm.h"
#include "../inc/rtm_mng.h"
#include "../inc/ekeying.h"
#include "../inc/sdr.h"
#include "../inc/fru.h"
#include "../inc/ipmi_if.h"

unsigned short amc_get_fru_size(){
	unsigned short size = 0;
	
	//Calcul of FRU_SIZE
	size = 8*read_eeprom_byte(5);	//Read the record area info offset (FRU HEADER byte 5)
	
	if(size){					//If record area info is not empty
		while(!(read_eeprom_byte(size+1) & 0x80)){
			size += read_eeprom_byte(size+2) + 5;
		}
		size += read_eeprom_byte(size+2) + 5;
	}else if((size = 8*read_eeprom_byte(4))){
		size += read_eeprom_byte(size+1)*8;
	}else if((size = 8*read_eeprom_byte(3))){
		size += read_eeprom_byte(size+1)*8;
	}else
		size = 0;
	
	return size;
}

//*******************************************/
unsigned char ipmi_get_fru_inventory_area_info(unsigned char fru, unsigned char* buf) //Called from ipmi_if.c
//*******************************************/
{
    unsigned char len = 0;
	unsigned short size;
	
	if(fru == AMC)
		size = 	amc_get_fru_size();
	#ifdef USE_RTM
		else if(fru == RTM){
			if(get_fru_size() ==  0)	size = 128;		//Not initialized yet
			size = get_fru_size();
		}
	#endif
	else
		return 0xff;
		
	buf[len++] = size & 0xFF;
	buf[len++] = size >> 8;
	
    buf[len++] = 0x00; //byte access type

    return (len);
}

//**************************************************/
unsigned char ipmi_fru_data_read(unsigned char fru, unsigned char* area, unsigned char len, unsigned char* data) //Called from ipmi_if.c
//**************************************************/
{
    unsigned short address = 0;
    unsigned char i;

    if (len > MAX_BYTES_READ)
        return (0xff);

    address = area[0] | (area[1] << 8);

    *data++ = len;

	if(fru == AMC){
		for (i = 0; i < len; i++)
			*(data + i) = read_eeprom_byte(address + i);
	}
	#ifdef USE_RTM
		else if(fru == RTM){
			ext_i2c_received(RTM_EEPROM_ADDR, address, 2, len, data);
		}
	#endif
    return(len + 1);   //MJ ipmi_if.c only looks at errors (0xff). Maybe it would be cleaner to return 0
}


//***************************************************/
unsigned char ipmi_fru_data_write(unsigned char* area, unsigned char* data, unsigned char len) //Called from ipmi_if.c
//***************************************************/
{
    unsigned short address;
    unsigned char i;

    address = area[0] | (area[1] << 8);

    if ((address + len) > MAX_FRU_SIZE)
        return(0xff);

    for (i = 0; i < len; i++)
        write_eeprom_byte(address + i, data[i]);

    return(len);    //MJ ipmi_if.c only looks at errors (0xff). Maybe it would be cleaner to return 0
}

#if defined(PORTCONF_CNT) || defined(PORTCONF_LIST)
	PortState_t amc_port_state[PORTCONF_CNT];
#endif

unsigned char set_channel_init(unsigned char port, unsigned char type, unsigned char ext, unsigned char state){
	#if defined(PORTCONF_CNT) || defined(PORTCONF_LIST)
		unsigned short offset = 0;
		unsigned char oem_guid_cnt = 0;
		unsigned char oem_ch_desc_cnt = 0;
		unsigned char chPort = 0;
		unsigned char channelID = 0;
	
		//Calcul of FRU_SIZE
		offset = 8*read_eeprom_byte(5);	//Read the record area info offset (FRU HEADER byte 5)
	
		if(offset){					//If record area info is not empty
			while(!(read_eeprom_byte(offset+1) & 0x80) && read_eeprom_byte(offset+8) != 0x19){
				offset += read_eeprom_byte(offset+2) + 5;
			}
			if(read_eeprom_byte(offset+8) == 0x19){
				oem_guid_cnt = read_eeprom_byte(offset+10);
				oem_ch_desc_cnt = read_eeprom_byte(offset+12+16*oem_guid_cnt);
			
				if(channelID >= oem_ch_desc_cnt)	return 0x00;
			
				do{
					//Get port associated to this channelID
					chPort = read_eeprom_byte(offset+13+16*oem_guid_cnt+channelID*3) & 0x1F;	//Port lane0
					channelID++;
				}while(chPort != port);
			
				if(chPort != port)	return 0xFF;
			
				channelID--;
			
				return ipmi_picmg_port_state_set(channelID, type, ext, 0, state);
			}else
				return 0xFF;
		}else
			return 0xFF;
	
		return 0x00;
	#else
		return 0xFF;
	#endif
}

unsigned char ipmi_picmg_port_state_get(unsigned char channelID, unsigned char* buf){
	#if defined(PORTCONF_CNT) || defined(PORTCONF_LIST)
		if(channelID >= PORTCONF_CNT)	return 0xFF;
		
		buf[0] = ((amc_port_state[channelID].protocol & 0x0F) << 4) | 0x01;
		buf[1] = ((amc_port_state[channelID].ext & 0x0F) << 4) | ((amc_port_state[channelID].protocol & 0xF0) >> 4);
		buf[2] = amc_port_state[channelID].groupID;
		buf[3] = amc_port_state[channelID].state;
		
		return 4;
	#else
		return 0;
	#endif
}

//***********************************************/
unsigned char ipmi_picmg_port_state_set(unsigned char channelID, unsigned char type, unsigned char ext, unsigned char groupID, unsigned char state){	
	#if defined(PORTCONF_CNT) || defined(PORTCONF_LIST)
		unsigned short offset = 0;
		unsigned char oem_guid_cnt = 0;
		unsigned char oem_ch_desc_cnt = 0;
		unsigned char chPort = 0;
		unsigned char ret = FAILED;
	
		//Calcul of FRU_SIZE
		offset = 8*read_eeprom_byte(5);	//Read the record area info offset (FRU HEADER byte 5)
	
		if(offset){					//If record area info is not empty
			while(!(read_eeprom_byte(offset+1) & 0x80) && read_eeprom_byte(offset+8) != 0x19){
				offset += read_eeprom_byte(offset+2) + 5;
			}
			if(read_eeprom_byte(offset+8) == 0x19){
				oem_guid_cnt = read_eeprom_byte(offset+10);
				oem_ch_desc_cnt = read_eeprom_byte(offset+12+16*oem_guid_cnt);
			
				if(channelID >= oem_ch_desc_cnt)	return 0x00;
			
				//Get port associated to this channelID
				chPort = read_eeprom_byte(offset+13+16*oem_guid_cnt+channelID*3) & 0x1F;	//Port lane0
			
				if(state == P2P_ENABLE)	ret = port_ekeying_enable(chPort, type, ext);
				else if(state == P2P_DISABLE) ret = port_ekeying_disable(chPort, type, ext);
			
				amc_port_state[channelID].channelID = channelID;
				amc_port_state[channelID].port = chPort;
				amc_port_state[channelID].ext = ext;
				amc_port_state[channelID].protocol = type;
				amc_port_state[channelID].groupID = groupID;
				if(ret == SUCCESS)	amc_port_state[channelID].state = state;
			}else
				return 0xFF;
		}else
			return 0xFF;
		
		return 0x00;
	#else
		return 0x00;
	#endif
}

//***********************************************/
unsigned char ipmi_picmg_set_clock_state(unsigned char clockID, unsigned char setting){
	//unsigned char ret = FAILED;
	
	//set_sensor_value(11,1); // 11 = ekeyign sensor
	
	//if((setting & 0x80) == CLK_ENABLE)	clock_ekeying_enable(clockID);
	//else if((setting & 0x80) == CLK_DISABLE) clock_ekeying_disable(clockID);
	
	//if(ret == SUCCESS)	amc_port_state[channelID] = state;
	
	return 0;	
}

//The PICMG properties are defined in ATCA 3.0, Table 3-11 (see errata)
//MJ: For AMCs the definitions in the AMC standard override ATCA 3.0. Therefore table 3-1 on page 3-4 of AMC R2.0 applies
//MJ: Finally the "Maximum FRU Device ID" has to be "1" according to MTCA.4
//********************************/
unsigned char ipmi_picmg_properties(unsigned char* buf) //Called from ipmi_if.c
//********************************/
{
    unsigned char len = 0;
    //set_led_pattern(1, 0xff, 2);

    buf[len++] = 0x14;  // version of the PICMG extensions
	#ifdef USE_RTM
		buf[len++] = 0x01;  // Maximum FRU Device ID
	#else
		buf[len++] = 0x00;
	#endif
    buf[len++] = 0x00;  // FRU Device ID

    return(len);
}

unsigned char activation_locked = 0x00, deactivation_locked = 0x00;

unsigned char ipmi_set_fru_policy(unsigned char mask_bits, unsigned char set_bits){	//JM : Set FRU Activation Policy command modifies the way a FRU's Operation State transitions behave
	if(mask_bits & 0x02){
		deactivation_locked = (set_bits & 0x02) >> 1;	//JM : Set deactivation_locked value as specified
	}else if(mask_bits & 0x01){
		activation_locked = (set_bits & 0x01);			//JM : Set activation_locked value as specified
	}else
		return 0xff;
		
	return 0x00;
}

unsigned char testVal;

unsigned char ipmi_get_fru_policy(){	//JM : Get FRU Activation Policy command provide a method for querying the current settings of the FRU Activation Policy
	testVal = (0x00 | ((deactivation_locked << 1) & 0x02) | activation_locked);
	return (0x00 | ((deactivation_locked << 1) & 0x02) | activation_locked);
}

unsigned char activate_fru_cmd_received = 0, deactivate_fru_cmd_received = 0;

unsigned char ipmi_set_fru_activation(unsigned char fru_activation_deactivation){	//JM : Set FRU Activation command
	
	if(fru_activation_deactivation == 0x01){
		activate_fru_cmd_received++;
	}
	else if(deactivate_fru_cmd_received == 0x00){
		deactivate_fru_cmd_received++;
	}
	
	return 0x00;
}

void fru_init(){
	//Write FRU information (only if the memory is empty)
	#ifndef FORCE_WRITE_FRU
		if (read_eeprom_byte(0) != 0xff)   return;
	#endif
	
		write_fru_binary();
}