/*
 * frueditor.h
 *
 * Created: 24/03/2015 16:56:32
 *  Author: jumendez
 */ 
#define FRU_EDITOR_ENABLE

typedef unsigned char u08;
typedef unsigned int u32;

#define AMC_MODULE		0x80
#define PCIE			0x02
#define LANE_NOT_USED	0xFF
#define LANE0_FLAG		0x01
#define LANE1_FLAG		0x02
#define LANE2_FLAG		0x04
#define LANE3_FLAG		0x08
#define NO_EXTENSION	0x00
#define OPERATES_ALONE	0x00

#define EXACT_MATCHES	0x00
#define MATCHES_01		0x01
#define MATCHES_10		0x02

#define PORT(n) 		n
	
#ifdef FRU_EDITOR_ENABLE
	#ifndef FRUEDITOR_H
	#define FRUEDITOR_H

	void light_amc_point_to_point_record_func(void);
	void module_current_record_function(void);
	void board_info_area_function(void);
	void product_info_area_function(void);
	void point_to_point_clock(void);
	void fru_header_function(void);
	void free_fru_binary(void);
	void write_fru_binary();

	#endif

	//Module Current record (SAVE)
	#ifdef MODULE_CURRENT_RECORD_SAVE
		#define current_in_ma(curr)		(u08)(curr/100);
	#endif

	//AMC Point to point connectivity record (LIGHT RECORD SAVE)#ifdef OEM_GUID_MODULE_POINT_TO_POINT_RECORD_GENERATE	
	#ifdef OEM_GUID_MODULE_POINT_TO_POINT_RECORD_GENERATE
		#undef OEM_GUID
		#define OEM_GUID(v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15)			\
			amc_point_to_point_record[i++] = v0;										\
			amc_point_to_point_record[i++] = v1;										\
			amc_point_to_point_record[i++] = v2;										\
			amc_point_to_point_record[i++] = v3;										\
			amc_point_to_point_record[i++] = v4;										\
			amc_point_to_point_record[i++] = v5;										\
			amc_point_to_point_record[i++] = v6;										\
			amc_point_to_point_record[i++] = v7;										\
			amc_point_to_point_record[i++] = v8;										\
			amc_point_to_point_record[i++] = v9;										\
			amc_point_to_point_record[i++] = v10;										\
			amc_point_to_point_record[i++] = v11;										\
			amc_point_to_point_record[i++] = v12;										\
			amc_point_to_point_record[i++] = v13;										\
			amc_point_to_point_record[i++] = v14;										\
			amc_point_to_point_record[i++] = v15;
	#endif
	
	#ifdef CHANNEL_DESCRIPTOR_MODULE_POINT_TO_POINT_RECORD_GENERATE
		#undef OEM_POINT_TO_POINT_RECORD
		#undef GENERIC_POINT_TO_POINT_RECORD
		
		#define OEM_POINT_TO_POINT_RECORD(id, port, oem_id, matches)						\
			ch_descriptor[id][0] = 0xE0 | port;												\
			ch_descriptor[id][1] = 0xFF;													\
			ch_descriptor[id][2] = 0xFF;
			
		#define GENERIC_POINT_TO_POINT_RECORD(id, port, protocol, protocol_extension, matches)	\
			ch_descriptor[id][0] = 0xE0 | port;													\
			ch_descriptor[id][1] = 0xFF;														\
			ch_descriptor[id][2] = 0xFF;
	#endif
	
	#ifdef LINK_DESCRIPTOR_MODULE_POINT_TO_POINT_RECORD_GENERATE
		#undef OEM_POINT_TO_POINT_RECORD
		#undef GENERIC_POINT_TO_POINT_RECORD
		
		#define OEM_POINT_TO_POINT_RECORD(id, port, oem_id, matches)						\
			link_descriptor[id][0] = id;													\
			link_descriptor[id][1] = ((oem_id & 0x0F) << 4) | 0x01;							\
			link_descriptor[id][2] = 0x0F;													\
			link_descriptor[id][3] = 0x00;													\
			link_descriptor[id][4] = 0xFC | matches;
		
		#define GENERIC_POINT_TO_POINT_RECORD(id, port, protocol, protocol_extension, matches)	\
			link_descriptor[id][0] = id;														\
			link_descriptor[id][1] = ((protocol & 0x0F) << 4) | 0x01;							\
			link_descriptor[id][2] = ((protocol_extension & 0x0F) << 4) & 0xF0;					\
			link_descriptor[id][3] = 0x00;														\
			link_descriptor[id][4] = 0xFC | matches;
	#endif
	
	//AMC Clock Configuration record (INIT)
	#ifdef AMC_POINT_TO_POINT_CLOCK_INIT
		#undef INDIRECT_CLOCK_CONNECTION
		#undef DIRECT_CLOCK_CONNECTION

		#define INDIRECT_CLOCK_CONNECTION(connection_id, clock_id, clock_activation_control, PLL, clock_source_receiver, dependant_clock_id)									\
			u08 indirect_ ## connection_id ## _ ## clock_id ## _ ## clock_activation_control ## _array[] = {(PLL << 1)| clock_source_receiver, dependant_clock_id};

		#define DIRECT_CLOCK_CONNECTION(connection_id, clock_id, clock_activation_control, PLL, clock_source_receiver, clock_family, clock_accuracy, freq_Hz, min_Hz, max_Hz)	\
			u08 direct_ ## connection_id ## _ ## clock_id ## _ ## clock_activation_control ## _array[] = {																		\
				(PLL << 1)| clock_source_receiver, 																																\
				clock_family, clock_accuracy, 																																	\
				(u08)(((u32)freq_Hz) & 0xFF), 																															\
				(u08)((((u32)freq_Hz) & 0xFF00) >> 8), 																												\
				(u08)((((u32)freq_Hz) & 0xFF0000) >> 16), 																												\
				(u08)((((u32)freq_Hz) & 0xFF000000) >> 24),																											\
				(u08)(((u32)min_Hz) & 0xFF), 																															\
				(u08)((((u32)min_Hz) & 0xFF00) >> 8), 																													\
				(u08)((((u32)min_Hz) & 0xFF0000) >> 16), 																												\
				(u08)((((u32)min_Hz) & 0xFF000000) >> 24),																												\
				(u08)(((u32)max_Hz) & 0xFF), 																															\
				(u08)((((u32)max_Hz) & 0xFF00) >> 8), 																													\
				(u08)((((u32)max_Hz) & 0xFF0000) >> 16), 																												\
				(u08)((((u32)max_Hz) & 0xFF000000) >> 24),																												\
			};
	#endif

	//AMC Clock Configuration record (SAVE)
	#ifdef AMC_POINT_TO_POINT_CLOCK_SAVE
		#undef INDIRECT_CLOCK_CONNECTION
		#undef DIRECT_CLOCK_CONNECTION

		#define INDIRECT_CLOCK_CONNECTION(connection_id, clock_id, clock_activation_control, PLL, clock_source_receiver, dependant_clock_id)													\
			clock_descriptor_exist[(clock_id-1) + (clock_activation_control*5)] =  1;																											\
			clock_descriptor[(clock_id-1) + (clock_activation_control*5)][0] =  clock_id;																										\
			clock_descriptor[(clock_id-1) + (clock_activation_control*5)][1] =  clock_activation_control;																						\
			indirect_connection_array[(clock_id-1) + (clock_activation_control*5)][connection_id] = indirect_ ## connection_id ## _ ## clock_id ## _ ## clock_activation_control ## _array;

		#define DIRECT_CLOCK_CONNECTION(connection_id, clock_id, clock_activation_control, PLL, clock_source_receiver, clock_family, clock_accuracy, freq_Hz, min_Hz, max_Hz)					\
			clock_descriptor_exist[(clock_id-1) + (clock_activation_control*5)] =  1;																											\
			clock_descriptor[(clock_id-1) + (clock_activation_control*5)][0] =  clock_id;																										\
			clock_descriptor[(clock_id-1) + (clock_activation_control*5)][1] =  clock_activation_control;																						\
			direct_connection_array[(clock_id-1) + (clock_activation_control*5)][connection_id] = direct_ ## connection_id ## _ ## clock_id ## _ ## clock_activation_control ## _array;
	#endif
#endif