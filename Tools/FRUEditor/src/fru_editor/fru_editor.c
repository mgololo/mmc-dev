/*
 * frueditor.c
 *
 * Created: 24/03/2015 17:01:48
 *  Author: jumendez
 */ 
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "fru.h"
#include "include/fru_info.h"

#define FRU_EDITOR_ENABLE

#ifdef FRU_EDITOR_ENABLE

	#ifdef 	LIGHT_AMC_POINT_TO_POINT_RECORD_LIST
		#ifndef LIGHT_AMC_POINT_TO_POINT_RECORD_CNT
			#error "LIGHT_AMC_POINT_TO_POINT_RECORD_CNT should be defined"
		#endif
		
		u08 * amc_point_to_point_record;
	#endif

	#ifdef MODULE_CURRENT_RECORD
		u08 module_current_record[11];
	#else
		#error "At least one module current record should be defined"
	#endif

	#ifdef BOARD_INFO_AREA_ENABLE
		u08 *board_info_area;
	#endif

	#ifdef PRODUCT_INFO_AREA_ENABLE
		u08 *product_info_area;
	#endif

	#ifdef AMC_POINT_TO_POINT_CLOCK_LIST
		#ifndef AMC_POINT_TO_POINT_CLOCK_CNT
			#error "AMC_POINT_TO_POINT_CLOCK_CNT should be defined"
		#endif
		u08 *point_to_point_clock_record;	
	#endif

	u08 fru_header[8];

	u08 checksum_clc(u08* buf, u08 length){
		u08 crc = 0x00;

		while (length--)
		crc += buf[length];

		return (0 - crc);
	}

	u08 checsum_verify(u08* buf, u08 length, u08 checksum){
		return (checksum - checksum_clc(buf, length));
	}

	void fru_header_function(void){
	
		u08 next_offset = 0x01;	//Header
	
		fru_header[0] = 0x01;
	
		#ifdef INTERNAL_USE_AREA_ENABLE
			fru_header[1] = 0x00; //JM: Should be modified
		#else
			fru_header[1] = 0x00;
		#endif
	
		#ifdef CHASSIS_INFO_AREA_ENABLE
			fru_header[2] = 0x00; //JM: Should be modified
		#else
			fru_header[2] = 0x00;
		#endif
	
		#ifdef BOARD_INFO_AREA_ENABLE
			fru_header[3] = next_offset;
			next_offset += board_info_area[1];
		#else
			fru_header[3] = 0x00;
		#endif
	
		#ifdef PRODUCT_INFO_AREA_ENABLE
			fru_header[4] = next_offset;
			next_offset += product_info_area[1];
		#else
			fru_header[4] = 0x00;
		#endif
	
		#ifdef MULTIRECORD_AREA_ENABLE
			fru_header[5] = next_offset;
		#else
			fru_header[5] = 0x00;
		#endif
	
		fru_header[6] = 0x00;
	
		fru_header[7] = checksum_clc(fru_header, 8);	
	}

	void board_info_area_function(void){
		#ifdef BOARD_INFO_AREA_ENABLE
			int i;
		
			u08 length = 13 + strlen(BOARD_MANUFACTURER) +
				strlen(BOARD_NAME) +
				strlen(BOARD_SN) +
				strlen(BOARD_PN) +
				strlen(FRU_FILE_ID);
		
			while(length%8)
				length++;
		
			board_info_area = malloc(sizeof(u08) * length);
		
			for(i=0; i < length; i++)
				board_info_area[i] = 0;
		
			board_info_area[0] = 0x01;
			board_info_area[1] = length/8;
			board_info_area[2] = LANG_CODE;
			board_info_area[3] = 0x00;
			board_info_area[4] = 0x00;
			board_info_area[5] = 0x00;
		
			board_info_area[6] = 0xC0 | (strlen(BOARD_MANUFACTURER) & 0x3F);
			for(i=0; i < strlen(BOARD_MANUFACTURER); i++)
				board_info_area[7+i] = BOARD_MANUFACTURER[i];
		
			board_info_area[7+strlen(BOARD_MANUFACTURER)] = 0xC0 | (strlen(BOARD_NAME) & 0x3F);
			for(i=0; i < strlen(BOARD_NAME); i++)
				board_info_area[8+strlen(BOARD_MANUFACTURER)+i] = BOARD_NAME[i];
		
			board_info_area[8+strlen(BOARD_MANUFACTURER)+strlen(BOARD_NAME)] = 0xC0 | (strlen(BOARD_SN) & 0x3F);
			for(i=0; i < strlen(BOARD_SN); i++)
				board_info_area[9+strlen(BOARD_MANUFACTURER)+strlen(BOARD_NAME)+i] = BOARD_SN[i];
		
			board_info_area[9+strlen(BOARD_MANUFACTURER)+strlen(BOARD_NAME)+strlen(BOARD_SN)] = 0xC0 | (strlen(BOARD_PN) & 0x3F);
			for(i=0; i < strlen(BOARD_PN); i++)
				board_info_area[10+strlen(BOARD_MANUFACTURER)+strlen(BOARD_NAME)+strlen(BOARD_SN)+i] = BOARD_PN[i];
		
			board_info_area[10+strlen(BOARD_MANUFACTURER)+strlen(BOARD_NAME)+strlen(BOARD_SN)+strlen(BOARD_PN)] = 0xC0 | (strlen(FRU_FILE_ID) & 0x3F);
			for(i=0; i < strlen(FRU_FILE_ID); i++)
				board_info_area[11+strlen(BOARD_MANUFACTURER)+strlen(BOARD_NAME)+strlen(BOARD_SN)+strlen(BOARD_PN)+i] = FRU_FILE_ID[i];
		
			board_info_area[11+strlen(BOARD_MANUFACTURER)+strlen(BOARD_NAME)+strlen(BOARD_SN)+strlen(BOARD_PN)+strlen(FRU_FILE_ID)] = 0xC1;
		
			board_info_area[length-1] = checksum_clc(board_info_area, length);
		#endif
	}

	void product_info_area_function(void){
		#ifdef PRODUCT_INFO_AREA_ENABLE
			int i;
			u08 length = 12 + strlen(PRODUCT_MANUFACTURER) +
			strlen(PRODUCT_NAME) +
			strlen(PRODUCT_PN) +
			strlen(PRODUCT_VERSION) +
			strlen(PRODUCT_SN) +
			strlen(PRODUCT_ASSET_TAG) +
			strlen(FRU_FILE_ID);
		
			while(length%8)
			length++;
		
			product_info_area = malloc(sizeof(u08) * length);
		
			for(i=0; i < length; i++)
			product_info_area[i] = 0;
		
			product_info_area[0] = 0x01;
			product_info_area[1] = length/8;
			product_info_area[2] = LANG_CODE;
		
			product_info_area[3] = 0xC0 | (strlen(PRODUCT_MANUFACTURER) & 0x3F);
			for(i=0; i < strlen(PRODUCT_MANUFACTURER); i++)
			product_info_area[4+i] = PRODUCT_MANUFACTURER[i];
		
			product_info_area[4+strlen(PRODUCT_MANUFACTURER)] = 0xC0 | (strlen(PRODUCT_NAME) & 0x3F);
			for(i=0; i < strlen(PRODUCT_NAME); i++)
			product_info_area[5+strlen(PRODUCT_MANUFACTURER)+i] = PRODUCT_NAME[i];
		
			product_info_area[5+strlen(PRODUCT_MANUFACTURER)+strlen(PRODUCT_NAME)] = 0xC0 | (strlen(PRODUCT_PN) & 0x3F);
			for(i=0; i < strlen(PRODUCT_PN); i++)
			product_info_area[6+strlen(PRODUCT_MANUFACTURER)+strlen(PRODUCT_NAME)+i] = PRODUCT_PN[i];
		
			product_info_area[6+strlen(PRODUCT_MANUFACTURER)+strlen(PRODUCT_NAME)+strlen(PRODUCT_PN)] = 0xC0 | (strlen(PRODUCT_VERSION) & 0x3F);
			for(i=0; i < strlen(PRODUCT_VERSION); i++)
			product_info_area[7+strlen(PRODUCT_MANUFACTURER)+strlen(PRODUCT_NAME)+strlen(PRODUCT_PN)+i] = PRODUCT_VERSION[i];
		
			product_info_area[7+strlen(PRODUCT_MANUFACTURER)+strlen(PRODUCT_NAME)+strlen(PRODUCT_PN)+strlen(PRODUCT_VERSION)] = 0xC0 | (strlen(PRODUCT_SN) & 0x3F);
			for(i=0; i < strlen(PRODUCT_SN); i++)
			product_info_area[8+strlen(PRODUCT_MANUFACTURER)+strlen(PRODUCT_NAME)+strlen(PRODUCT_PN)+strlen(PRODUCT_VERSION)+i] = PRODUCT_SN[i];
		
			product_info_area[8+strlen(PRODUCT_MANUFACTURER)+strlen(PRODUCT_NAME)+strlen(PRODUCT_PN)+strlen(PRODUCT_VERSION)+strlen(PRODUCT_SN)] = 0xC0 | (strlen(PRODUCT_ASSET_TAG) & 0x3F);
			for(i=0; i < strlen(PRODUCT_ASSET_TAG); i++)
			product_info_area[9+strlen(PRODUCT_MANUFACTURER)+strlen(PRODUCT_NAME)+strlen(PRODUCT_PN)+strlen(PRODUCT_VERSION)+strlen(PRODUCT_SN)+i] = PRODUCT_ASSET_TAG[i];
		
			product_info_area[9+strlen(PRODUCT_MANUFACTURER)+strlen(PRODUCT_NAME)+strlen(PRODUCT_PN)+strlen(PRODUCT_VERSION)+strlen(PRODUCT_SN)+strlen(PRODUCT_ASSET_TAG)] = 0xC0 | (strlen(FRU_FILE_ID) & 0x3F);
			for(i=0; i < strlen(FRU_FILE_ID); i++)
			product_info_area[10+strlen(PRODUCT_MANUFACTURER)+strlen(PRODUCT_NAME)+strlen(PRODUCT_PN)+strlen(PRODUCT_VERSION)+strlen(PRODUCT_SN)+strlen(PRODUCT_ASSET_TAG)+i] = FRU_FILE_ID[i];
		
			product_info_area[10+strlen(PRODUCT_MANUFACTURER)+strlen(PRODUCT_NAME)+strlen(PRODUCT_PN)+strlen(PRODUCT_VERSION)+strlen(PRODUCT_SN)+strlen(PRODUCT_ASSET_TAG)+strlen(FRU_FILE_ID)] = 0xC1;
		
			product_info_area[length-1] = checksum_clc(product_info_area, length);
		#endif	
	}


	void module_current_record_function(void){
		#ifdef MODULE_CURRENT_RECORD		
		
			#define MODULE_CURRENT_RECORD_SAVE
				#include "fru.h"
			#undef MODULE_CURRENT_RECORD_SAVE
			
			module_current_record[0] = 0xC0;
			module_current_record[1] = 0x82;
			module_current_record[2] = 0x06;
			module_current_record[3] = 0x00;
			module_current_record[4] = 0x00;
			module_current_record[5] = 0x5a;
			module_current_record[6] = 0x31;
			module_current_record[7] = 0x00;
			module_current_record[8] = 0x16;
			module_current_record[9] = 0x00;
			module_current_record[10]= MODULE_CURRENT_RECORD;
			module_current_record[3] = checksum_clc(&(module_current_record[5]), 6);		//record checksum
			module_current_record[4] = checksum_clc(module_current_record, 5);				//Header checksum
		#endif
	}
	
	void light_amc_point_to_point_record_func(){
		
		#ifdef LIGHT_AMC_POINT_TO_POINT_RECORD_LIST
			
			#ifndef POINT_TO_POINT_OEM_GUID_CNT
				#define POINT_TO_POINT_OEM_GUID_CNT		0
			#endif
			
			u08 ch_descriptor[LIGHT_AMC_POINT_TO_POINT_RECORD_CNT][3];
			u08 link_descriptor[LIGHT_AMC_POINT_TO_POINT_RECORD_CNT][5];
			
			amc_point_to_point_record = (u08 *)malloc(14+POINT_TO_POINT_OEM_GUID_CNT*16+LIGHT_AMC_POINT_TO_POINT_RECORD_CNT*3+LIGHT_AMC_POINT_TO_POINT_RECORD_CNT*5);
			
			u08 i;
			
			amc_point_to_point_record[0] = 0xC0;	//OEM Record
			amc_point_to_point_record[1] = 0x02;	//Record format version
			amc_point_to_point_record[2] = 9+POINT_TO_POINT_OEM_GUID_CNT*16+LIGHT_AMC_POINT_TO_POINT_RECORD_CNT*3+LIGHT_AMC_POINT_TO_POINT_RECORD_CNT*5;	//Record length
			amc_point_to_point_record[3] = 0x00;	//Record checksum (calculate at the end)
			amc_point_to_point_record[4] = 0x00;	//Header checksum (calculate at the end)
			amc_point_to_point_record[5] = 0x5A;	//Manufacturer ID (PICMG)
			amc_point_to_point_record[6] = 0x31;	//Manufacturer ID (PICMG)
			amc_point_to_point_record[7] = 0x00;	//Manufacturer ID (PICMG)
			amc_point_to_point_record[8] = 0x19;	//PICMG Record ID (19h - AMC Point to point connectivity)
			amc_point_to_point_record[9] = 0x00;	//Record format version (0x00 for this version)
			amc_point_to_point_record[10]= POINT_TO_POINT_OEM_GUID_CNT;	//OEM Guid cnt
			
			i = 11;
			#ifdef POINT_TO_POINT_OEM_GUID_LIST
				#define OEM_GUID_MODULE_POINT_TO_POINT_RECORD_GENERATE
				#include "fru.h"
					POINT_TO_POINT_OEM_GUID_LIST
				#undef OEM_GUID_MODULE_POINT_TO_POINT_RECORD_GENERATE
			#endif
			
			amc_point_to_point_record[11+16*POINT_TO_POINT_OEM_GUID_CNT] = 0x80;	//AMC Module
			amc_point_to_point_record[12+16*POINT_TO_POINT_OEM_GUID_CNT] = LIGHT_AMC_POINT_TO_POINT_RECORD_CNT;	//AMC Channel descriptor count
			
			#define CHANNEL_DESCRIPTOR_MODULE_POINT_TO_POINT_RECORD_GENERATE
				#include "fru.h"
				LIGHT_AMC_POINT_TO_POINT_RECORD_LIST
			#undef CHANNEL_DESCRIPTOR_MODULE_POINT_TO_POINT_RECORD_GENERATE
			
			for(i=0; i < LIGHT_AMC_POINT_TO_POINT_RECORD_CNT; i++){
				amc_point_to_point_record[13+16*POINT_TO_POINT_OEM_GUID_CNT+i*3]	= ch_descriptor[i][0];
				amc_point_to_point_record[13+16*POINT_TO_POINT_OEM_GUID_CNT+i*3+1]	= ch_descriptor[i][1];
				amc_point_to_point_record[13+16*POINT_TO_POINT_OEM_GUID_CNT+i*3+2]	= ch_descriptor[i][2];
			}
			
			#define LINK_DESCRIPTOR_MODULE_POINT_TO_POINT_RECORD_GENERATE
				#include "fru.h"
				LIGHT_AMC_POINT_TO_POINT_RECORD_LIST
			#undef LINK_DESCRIPTOR_MODULE_POINT_TO_POINT_RECORD_GENERATE
			
			for(i=0; i < LIGHT_AMC_POINT_TO_POINT_RECORD_CNT; i++){
				amc_point_to_point_record[13+16*POINT_TO_POINT_OEM_GUID_CNT+LIGHT_AMC_POINT_TO_POINT_RECORD_CNT*3+i*5]		= link_descriptor[i][0];
				amc_point_to_point_record[13+16*POINT_TO_POINT_OEM_GUID_CNT+LIGHT_AMC_POINT_TO_POINT_RECORD_CNT*3+i*5+1]	= link_descriptor[i][1];
				amc_point_to_point_record[13+16*POINT_TO_POINT_OEM_GUID_CNT+LIGHT_AMC_POINT_TO_POINT_RECORD_CNT*3+i*5+2]	= link_descriptor[i][2];
				amc_point_to_point_record[13+16*POINT_TO_POINT_OEM_GUID_CNT+LIGHT_AMC_POINT_TO_POINT_RECORD_CNT*3+i*5+3]	= link_descriptor[i][3];
				amc_point_to_point_record[13+16*POINT_TO_POINT_OEM_GUID_CNT+LIGHT_AMC_POINT_TO_POINT_RECORD_CNT*3+i*5+4]	= link_descriptor[i][4];
			}
			
			amc_point_to_point_record[3] = checksum_clc((amc_point_to_point_record+5), amc_point_to_point_record[2]);		//record checksum
			amc_point_to_point_record[4] = checksum_clc(amc_point_to_point_record, 5);										//Header checksum
			
		#endif
	}
	
	void point_to_point_clock(void){
		#ifdef AMC_POINT_TO_POINT_CLOCK_LIST
			int i, j, k;
	
			u08 reg = 0;
			u08 length = 0;
			u08 clk_descriptor_cnt = 0;
			u08 indirect_cnt = 0;
			u08 direct_cnt = 0;
	
			u08 clock_descriptor_exist[10];
			u08 clock_descriptor[10][4];
			u08 *indirect_connection_array[10][AMC_POINT_TO_POINT_CLOCK_CNT];
			u08 *direct_connection_array[10][AMC_POINT_TO_POINT_CLOCK_CNT];
	
			#define AMC_POINT_TO_POINT_CLOCK_INIT
			#include "fru.h"
				AMC_POINT_TO_POINT_CLOCK_LIST
			#undef AMC_POINT_TO_POINT_CLOCK_INIT
	
			for(i=0 ; i < 10; i++){
				clock_descriptor_exist[i] = 0;
		
				for(j=0; j < AMC_POINT_TO_POINT_CLOCK_CNT; j++){
					indirect_connection_array[i][j] = NULL;
					direct_connection_array[i][j] = NULL;
				}
		
				clock_descriptor[i][0] = 0;
				clock_descriptor[i][1] = 0;
				clock_descriptor[i][2] = 0;
				clock_descriptor[i][3] = 0;
			}
	
			#define AMC_POINT_TO_POINT_CLOCK_SAVE
			#include "fru.h"
				AMC_POINT_TO_POINT_CLOCK_LIST
			#undef AMC_POINT_TO_POINT_CLOCK_SAVE
	
			for(i=0, clk_descriptor_cnt=0, indirect_cnt=0, direct_cnt=0; i < 10; i++){
				if(clock_descriptor_exist[i]){	length += 4; clk_descriptor_cnt++; }
				for(j=0; j < AMC_POINT_TO_POINT_CLOCK_CNT; j++){
					if(indirect_connection_array[i][j] != NULL){ length += 2; indirect_cnt++; }
					if(direct_connection_array[i][j] != NULL){ length += 15; direct_cnt++; }
				}
			}
	
			length += 12;
	
			point_to_point_clock_record = (u08 *) malloc(sizeof(u08)*length);
	
			point_to_point_clock_record[reg++] = 0xC0;
			point_to_point_clock_record[reg++] = 0x02;
			point_to_point_clock_record[reg++] = length-5;
			point_to_point_clock_record[reg++] = 0x00;
			point_to_point_clock_record[reg++] = 0x00;
			point_to_point_clock_record[reg++] = 0x5A;
			point_to_point_clock_record[reg++] = 0x31;
			point_to_point_clock_record[reg++] = 0x00;
			point_to_point_clock_record[reg++] = 0x2D;
			point_to_point_clock_record[reg++] = 0x00;
			point_to_point_clock_record[reg++]= 0xFF;
			point_to_point_clock_record[reg++]= clk_descriptor_cnt;
	
			for(i=0; i<10; i++){
				if(clock_descriptor_exist[i]){
					point_to_point_clock_record[reg++] = clock_descriptor[i][0];
					point_to_point_clock_record[reg++] = clock_descriptor[i][1];
					point_to_point_clock_record[reg++] = indirect_cnt;
					point_to_point_clock_record[reg++] = direct_cnt;
			
					for(j=0; j < AMC_POINT_TO_POINT_CLOCK_CNT; j++){
						if(indirect_connection_array[i][j] != NULL){
							for(k=0; k < 2; k++)
							point_to_point_clock_record[reg++] = indirect_connection_array[i][j][k];
						}
					}
			
					for(j=0; j < AMC_POINT_TO_POINT_CLOCK_CNT; j++){
						if(direct_connection_array[i][j] != NULL){
							for(k=0; k < 15; k++)
							point_to_point_clock_record[reg++] = direct_connection_array[i][j][k];
						}
					}
				}
			}
	
			point_to_point_clock_record[3] = checksum_clc((point_to_point_clock_record+5), point_to_point_clock_record[2]);		//record checksum
			point_to_point_clock_record[4] = checksum_clc(point_to_point_clock_record, 5);					//Header checksum
		#endif
	}


	void free_fru_binary(void){
		u08 i;
		
		free(board_info_area);
		free(product_info_area);
		
		#ifdef LIGHT_AMC_POINT_TO_POINT_RECORD_LIST
		free(amc_point_to_point_record);
		#endif
		
		#ifdef AMC_POINT_TO_POINT_CLOCK_LIST
		free(point_to_point_clock_record);
		#endif
	}
	
	u08 print = 0;
	u08 write_file = 0;
	FILE *frubin;
	
	unsigned int size = 0;
	u08 *mem;
	
	void EEPROM_write(u08 addr, u08 data){
		if(print){
			printf("addr[%d] = %02x \n",addr,data);
			mem[addr] = data;
		}
		if(write_file){
			fwrite(&data, 1, 1, frubin);
			//printf("addr[%d] = %02x \n",addr,data);
			//mem[addr] = data;			
		}
		size++;
	}	
	
	u08 EEPROM_read(u08 addr){
		return mem[addr];
	}
	
	void mem_alloc(){
		mem = (u08 *) malloc(size);
	}
	
	void write_fru_binary(){
		u08 size = 0;
		u08 addr = 0;
	
		int i, j;
	
		board_info_area_function();
		product_info_area_function();	
		light_amc_point_to_point_record_func();	
		module_current_record_function();
		point_to_point_clock();
		fru_header_function();
		
		for(i=0; i < 8; i++){
			EEPROM_write(addr++, fru_header[i]);
		}
	
		for(i=0; i < board_info_area[1]*8; i++){
			EEPROM_write(addr++, board_info_area[i]);
		}
	
		for(i=0; i < product_info_area[1]*8; i++){
			EEPROM_write(addr++, product_info_area[i]);
		}
	
		#ifdef LIGHT_AMC_POINT_TO_POINT_RECORD_LIST
			for(j=0; j < (amc_point_to_point_record[2]+5); j++){
				EEPROM_write(addr++, amc_point_to_point_record[j]);
			}
		#endif
	
		#ifdef AMC_POINT_TO_POINT_CLOCK_LIST
			for(j=0; j < (point_to_point_clock_record[2]+5); j++){
				EEPROM_write(addr++, point_to_point_clock_record[j]);
			}
		#endif
		
		for(j=0; j < (module_current_record[2]+5); j++){
			EEPROM_write(addr++, module_current_record[j]);
		}
		
		free_fru_binary();
	}
	
	void main(){
		
		frubin = fopen("fru.bin","wb");
		if(frubin == NULL){
			printf("{fru_editor} Error: File creation failed \n");
			return;
		}
		
		write_file = 1;
		write_fru_binary();	
	}
#endif