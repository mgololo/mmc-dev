//*************************************************************
// Copyright (C) 2015 CERN
//
// Edit  : Julian Mendez (julian.mendez@cern.ch)
//***************************************************************

General description
===================

FRU Editor: Generates an FRU information binary from an .h file that 
can also be used in the MMC application project. One option allows 
downloading the generated binary into the MMC EEPROM.

How to use
==========

Binary generation: 
 make generate INPUT="<path to fru_info header file>" OUTPUT="<binary name>"
 
Binary writing: 
 make write INPUT="<binary path/name>" IP="<MCH ip addr>" USERNAME="<MCH IPMI username>" PASSWORD="<MCH IPMI password>" SLOT="<AMC Slot number from 1 to 12>"
 
Generation and writing:
 make INPUT="<path to fru_info header file>" IP="<MCH ip addr>" USERNAME="<MCH IPMI username>" PASSWORD="<MCH IPMI password>" SLOT="<AMC Slot number from 1 to 12>"

SUPPORT
=======

NOTE: Please send feedback to julian.mendez@cern.ch if you need support