//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
//File Name	: mmc_main.c
// 
// Title		: mmc main file
// Revision		: 1.0
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
//
// Modified by  : Frederic Bompard, CPPM (Centre Physique des Particules de Marseille)
// Modified by  : Markus Joos (markus.joos@cern.ch)
//
// Description : The main routine for MMC(Module Management Controller).
//
// This code is distributed under the GNU Public License
// which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************
// Include Files
#include "../inc/ipmi_if.h"	    // ipmi interface
#include "../inc/hpm.h"

#include "../../drivers/drivers.h"


int main(void){

   if (get_startup_flag() == 0xFF){
	   write_startup_flag(0x00);				//If the byte is not overrode in the app, the boot loader start at next boot	   
	   watchdog_enable();						//Allows reboot to boot loader mode if application program does not reset or disable the watchdog after 2 secs (recovery mode)
	   (*((void(*)(void))PROGRAM_START_ADDR))();
   }
   
   watchdog_disable();
   
   boot_uC();
   
   hpm_init();
   ipmi_init();
      
   write_eeprom_byte(0, 0);	//Erase EEPROM -> Write 1st bit to 0xFF
   
   while(1){
		#ifdef I2C_POLLING_FUNC
			slave_i2c_poll();					//Wait for new command (Interrupt can't be used in boot loader)
		#endif
		
		ipmi_check_request();						//Check command received
		
		#ifdef I2C_POLLING_FUNC
			master_i2c_poll();			//Send response
		#endif
		
		manage_hpm();
   }
    
   return 0;
}