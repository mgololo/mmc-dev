/*
 *  Filename    : hpm.c
 *
 *  Title       : HPM.1 interface
 *  Revision    : 1.0
 *  Target MCU	: Atmel AVR series
 *  Description : HPM.1 interface functions
 *
 *  Created		: 18/10/2014
 *
 *  Author		: Julian Mendez <julian.mendez@cern.ch>
 */ 
#include <string.h>

#include "../inc/hpm.h"
#include "../inc/ipmi_if.h"

#include "../../drivers/drivers.h"

unsigned char cmd_in_progress, last_cmd_comp_code, comp_estimate, cmd_in_progress_data[25], cmd_in_progress_data_length; //Get status upgrade cmd informations
unsigned char upgradeFinished = 0;
unsigned char activateReceived = 0;

void hpm_init(){
	cmd_in_progress = INITIATE_UPGRADE_ACTION;
	cmd_in_progress_data_length = 1;
	cmd_in_progress_data[0] = UPGRADE_ACT_UPLOAD_FOR_UPGRADE;
	last_cmd_comp_code = 0x00;
}

unsigned char hpm_response_send(unsigned char cmd, unsigned char *data, unsigned char data_len, unsigned char *rsp_data, unsigned char *rsp_data_len){	//Return completion code
	
	unsigned char cc = IPMI_CC_OK;
	unsigned char i;
	unsigned int addr = 0x00;
	
	switch (cmd){
		
		case GET_UPGRADE_STATUS:
			
			rsp_data[0] = cmd_in_progress;
			rsp_data[1] = last_cmd_comp_code;
			rsp_data[2] = comp_estimate;
			
			* rsp_data_len = 3;
			
			break;
			
		case GET_TARGET_UPGRADE_CAPABILITIES:
			rsp_data[0] = 0x00;		//HPM.1 version
			rsp_data[1] = 0x38;		//Capabilities: Table 3-3 HPM.1 specification
			rsp_data[2] = 6;		//Upgrade timeout: 30 sec
			rsp_data[3] = 0x00;		//Self-test is not supported
			rsp_data[4] = 0x00;		//Rollback is not supported
			rsp_data[5] = 12;		//Inaccessibility timeout: 1 min
			rsp_data[6] = 0x01;		//Only one component
			
			*rsp_data_len = 7;
			
			break;
			
		case GET_COMPONENT_PROPERTIES:
			//if (data[1] != 0)								--JM: Comment -> ipmitool workaround
			//	return 0x82;	//Invalid component ID
			
			switch(data[2]){
				case GENERAL_COMP_PROPERTIES:
					rsp_data[0] = 0x34;		//General component properties
					*rsp_data_len = 1;
					break;
					
				case COMP_CURRENT_VERSION:
					rsp_data[0] = MMC_FW_REL_MAJ;	//Firmware revision (Major)
					rsp_data[1] = MMC_FW_REL_MIN;	//Firmware revision (Minor)
					for(i=2; i<6; i++)				//Auxilary Firmware Revision Information
						rsp_data[i] = 0x00;
						
					*rsp_data_len = 6;
					break;
					
				case COMP_DESCRIPTION:
					for (i=0; i<12 ; i++){
						if(i< strlen(COMP_DESC_STR))
							rsp_data[i] = COMP_DESC_STR[i];			
						else
							rsp_data[i] = 0;
					}
					
					*rsp_data_len = 12;
					
					break;
					
				case COMP_ROLLBACK_FW_VERSION:	//Not implemented yet
					return 0x83;
					
				case COMP_DEFERRED_UPG_FW_VERSION: //Not implemented yet
					return 0x83;	//Invalid component properties selector
					
				//case COMP_OEM_OFFSET:					//No OEM properties implemented yet
				
				default:
					return 0xcc;	//Invalid component properties selector
				
			}
		
			break;
			
		case INITIATE_UPGRADE_ACTION:
			//if (data[1] != 0x00){	//Invalid component					--JM: Comment -> ipmitool workaround
			//	return 0x81;
			/*}else*/ if (last_cmd_comp_code == 0x80){	//Command in progress
				return 0x80;
			}
			
			switch (data[2]){
				case UPGRADE_ACT_BACKUP_COMP: //Not implemented yet
					return 0xCC;
					break;
					
				case UPGRADE_ACT_PREPARE_COMP:	//Not implemented yet
					return 0x00;
					break;
					
				case UPGRADE_ACT_UPLOAD_FOR_UPGRADE:	//Not implemented yet
					flash_init();
					return 0x00;
					break;
					
				case UPGRADE_ACT_UPLOAD_FOR_COMPARE:	//Not implemented yet
					return 0xCC;
					break;
					
				default:
					return 0xCC;
			}
			
			break;
		
		case UPLOAD_FIRMWARE_BLOCK:
			cc = flash_write(data, data_len);
			*rsp_data_len = 0;
			break;
			
		case FINISH_FIRMWARE_UPLOAD:		
			if (data[1] != 0)
				return 0xcc;
			
			cc = flash_check((unsigned int)(((unsigned long)data[5]) << 24) | (((unsigned long)data[4]) << 16) | (((unsigned long)data[3]) << 8) | data[2]);
			if(cc == 0x00)		upgradeFinished = 1;
			else 				upgradeFinished = 2;
			*rsp_data_len = 0;
			
			break;
		
		case ACTIVE_FIRMWARE:
			if(upgradeFinished == 1){
				activateReceived = 1;
				return 0x00;	//Activation in progress
			}else
				return 0xCC;	//Impossible to activate firmware due to previous error
				
		case READ_FLASH_MEMORY:
			if(data_len < 5 || data[1] > 25)	// data[1]: len to be read in byte (max.: 25 bytes)
				return 0xcc;
			else{								// data [5:2]: addr pos of first byte to be read
				addr = (unsigned int)(((unsigned long)data[5]) << 24) | (((unsigned long)data[4]) << 16) | (((unsigned long)data[3]) << 8) | data[2];
				memcpy((void *)rsp_data, (const void *)addr, data[1]);
				*rsp_data_len = data[1];
				return 0x00;
			}
			
		default: 
			*rsp_data_len = 0;
			return 0xCC;
	}
	
	return cc;
}

void manage_hpm(){	//called from mmc_main.c - Execute long commands
	if(activateReceived){
		write_startup_flag(0xFF);
		reset_uC();
	}
}