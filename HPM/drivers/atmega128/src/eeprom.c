//*****************************************************************************
// Copyright (C): 2010 CPPM ( Centre Pysique des Particules de Marseille ) 
// File Name	: eeprom.c
// Date			:
// Title		: internal eeprom management
// Revision		: 2.0
// Notes		: 7/04/2010
// Target MCU	: Atmel AVR series
//
// Author       : Bompard frederic CPPM ( Centre Physique des Particules de Marseille )
// Modified by  : Paschalis Vichoudis, CERN
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
// Description : EEPROM management 
//					
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************
#include <avr/eeprom.h>

//*****************************************/
void write_eeprom_byte(unsigned short addr, unsigned char data){
	eeprom_write_byte((uint8_t *) (addr+1), data);
}

//***************************/
unsigned char read_eeprom_byte(unsigned short addr){
	return eeprom_read_byte((uint8_t*)(addr+1));
}

void write_startup_flag(unsigned char flag){
	eeprom_write_byte((uint8_t *) (0), flag);
}

unsigned char get_startup_flag(){
	return eeprom_read_byte((uint8_t *) (0));
}