/*! \file iodeclaration.c \ IO init*/
//*****************************************************************************
//
// File Name	: 'iodeclaration.c'
// Title		: IO init
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************

#include <avr/io.h>		    // include I/O definitions (port names, pin names, etc)

#include "../../../user/config.h"

#include "../inc/avrlibdefs.h"
#include "../inc/iodeclaration.h"
#include "../inc/pinout.h"


unsigned char get_signal(unsigned char id){
	
	switch(id & 0xF0){
		
		case 0x00:	//PORTA
			return (inb(PINA) & BV(id & 0x0F)) ? HIGH:LOW;
			break;
			
		case 0x10:	//PORTB
			return (inb(PINB) & BV(id & 0x0F)) ? HIGH:LOW;
			break;
			
		case 0x20:	//PORTC
			return (inb(PINC) & BV(id & 0x0F)) ? HIGH:LOW;
			break;
			
		case 0x30:	//PORTD
			return (inb(PIND) & BV(id & 0x0F)) ? HIGH:LOW;
			break;
			
		case 0x40:	//PORTE
			return (inb(PINE) & BV(id & 0x0F)) ? HIGH:LOW;
			break;
			
		case 0x50:	//PORTF
			return (inb(PINF) & BV(id & 0x0F)) ? HIGH:LOW;
			break;
			
		case 0x60:	//PORTG
			return (inb(PING) & BV(id & 0x0F)) ? HIGH:LOW;
			break;
	}
	
	return 0xFF;
}

void set_signal(unsigned char id){
	
	switch(id & 0xF0){
		
		case 0x00:	//PORTA
			sbi(PORTA, (id & 0x0F));
			break;
		
		case 0x10:	//PORTB
			sbi(PORTB, (id & 0x0F));
			break;
		
		case 0x20:	//PORTC
			sbi(PORTC, (id & 0x0F));
			break;
		
		case 0x30:	//PORTD
			sbi(PORTD, (id & 0x0F));
			break;
		
		case 0x40:	//PORTE
			sbi(PORTE, (id & 0x0F));
			break;
		
		case 0x50:	//PORTF
			sbi(PORTF, (id & 0x0F));
			break;
		
		case 0x60:	//PORTG
			sbi(PORTG, (id & 0x0F));
			break;
	}
}

void clear_signal(unsigned char id){
	
	switch(id & 0xF0){
		
		case 0x00:	//PORTA
			cbi(PORTA, (id & 0x0F));
			break;
		
		case 0x10:	//PORTB
			cbi(PORTB, (id & 0x0F));
			break;
		
		case 0x20:	//PORTC
			cbi(PORTC, (id & 0x0F));
			break;
		
		case 0x30:	//PORTD
			cbi(PORTD, (id & 0x0F));
			break;
			
		case 0x40:	//PORTE
			cbi(PORTE, (id & 0x0F));
			break;
			
		case 0x50:	//PORTF
			cbi(PORTF, (id & 0x0F));
			break;
		
		case 0x60:	//PORTG
			cbi(PORTG, (id & 0x0F));
			break;
	}
}

void set_signal_dir(unsigned char id, unsigned char inout){
		
	switch(id & 0xF0){
		
		case 0x00:	//PORTA
			if(inout)	cbi(DDRA, (id & 0x0F));
			else		sbi(DDRA, (id & 0x0F));
			break;
		
		case 0x10:	//PORTB
			if(inout)	cbi(DDRB, (id & 0x0F));
			else		sbi(DDRB, (id & 0x0F));
			break;
		
		case 0x20:	//PORTC
			if(inout)	cbi(DDRC, (id & 0x0F));
			else		sbi(DDRC, (id & 0x0F));
			break;
		
		case 0x30:	//PORTD
			if(inout)	cbi(DDRD, (id & 0x0F));
			else		sbi(DDRD, (id & 0x0F));
			break;
		
		case 0x40:	//PORTE
			if(inout)	cbi(DDRE, (id & 0x0F));
			else		sbi(DDRE, (id & 0x0F));
			break;
		
		case 0x50:	//PORTF
			if(inout)	cbi(DDRF, (id & 0x0F));
			else		sbi(DDRF, (id & 0x0F));
			break;
		
		case 0x60:	//PORTG
			if(inout)	cbi(DDRG, (id & 0x0F));
			else		sbi(DDRG, (id & 0x0F));
			break;
	}
}

//*****************/
void init_port(void)    //Only called once from mmc_main.c; could be in-lined  //MJ-VB: How much of this is user code?
//*****************/
{
	//Definition of DDR (Data Direction Register)
	//bit = 0: This line is an input line
	//bit = 1: This line is an output line
	//
	//Definition of PORT
	//If line is output and PORT bit = 0: Send logical 0
	//If line is output and PORT bit = 1: Send logical 1
	//If line is input and PORT bit = 0:  Disable the pull-up of this line
	//If line is input and PORT bit = 1:  Enable the pull-up of this line
	//
	//Ownership of the ports and their lines
	//G = This bit is part of the generic infrastructure
	//U = This bit is user defined
	//N = This bit is not used or does not exist
	//Example: GGGUUUNN
	//Bits 7-5 are "G"
	//Bits 4-2 are "U"
	//Bits 1-0 are "N"

	//The code below initialises the ports that contain "G" bits. User bits in these ports will be defined later in the user code section
	
	//set_signal_dir(LOCAL_LOW_VOLTAGE_POK,0x01);
	//set_signal_dir(RTM_PS,0x01);
	set_signal_dir(GA0,0x01);
	set_signal_dir(GA1,0x01);
	set_signal_dir(GA2,0x01);
	//set_signal_dir(LOCAL_FPGA2_INIT_DONE,0x01);
	//set_signal_dir(LOCAL_FPGA1_INIT_DONE,0x01);
	//set_signal_dir(LOCAL_HANDLE_SWITCH,0x01);
	//set_signal_dir(PS1,0x01);
	//set_signal_dir(PS0,0x01);
	//set_signal_dir(PRESENCE_12V,0x01);
	//set_signal_dir(MASTER_TDI,0x01);
	
	//set_signal_dir(RTM_12V_ENABLE,0x00);
	//set_signal_dir(RTM_3V3_ENABLE,0x00);
	//set_signal_dir(RTM_I2C_ENABLE,0x00);
	set_signal_dir(GA_PULLUP,0x00);
	//set_signal_dir(LOCAL_RED_LED,0x00);
	//set_signal_dir(LOCAL_GREEN_LED,0x00);
	//set_signal_dir(LOCAL_BLUE_LED,0x00);
	//set_signal_dir(LOCAL_RESET_FPGA,0x00);
	//set_signal_dir(LOCAL_RELOAD_FPGA,0x00);
	//set_signal_dir(LOCAL_REG_ENABLE,0x00);
	//set_signal_dir(LOCAL_DCDC_ENABLE,0x00);
	//set_signal_dir(MASTER_TCK,0x00);
	//set_signal_dir(MASTER_TMS,0x00);
	//set_signal_dir(MASTER_TDO,0x00);
}