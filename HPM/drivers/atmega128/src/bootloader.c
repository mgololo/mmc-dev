/*
 * bootloader.c
 *
 * Created: 18/10/2014 23:27:47
 *  Author: jumendez
 */

#include <avr/boot.h>
#include <avr/pgmspace.h>

#include "../../drivers.h"
#include "../inc/bootloader.h"

unsigned int FlashAddr;

/****************************************
HPM commands : HIGH LEVEL INTERFACE
*****************************************/
unsigned int image_size = 0, last_block = 0, tmpcnt = 0, bufptr;

unsigned char buf[SPM_PAGESIZE];

unsigned char buf_last_cmd[MAX_BYTES_READ+5], buf_last_cmd_length;

void flash_init(){		
	FlashAddr = 0;	
	bufptr = 0;
	image_size = 0;
	last_block = 0;
	tmpcnt = 0;
	bufptr = 0;
}

unsigned char flash_write(unsigned char *data, unsigned char data_len){	
		
		int i;	
		int block_size = 0;
		int cc = 0;		
		
		block_size = (data_len-2);
		
		if((tmpcnt == 0 && data[1] == 0x00) || data[1] != last_block){				
			image_size += (data_len-2);		
			
			for(i=0; i < block_size; i++){
				buf[bufptr++] = data[2+i]; //testwrite[i];
				
				//If we receive a new value and buffer is full
				if(bufptr >= SPM_PAGESIZE){								//Flash page full, write flash page;otherwise receive next frame
					cc = boot_program_page (FlashAddr, buf);			//write data to Flash
					FlashAddr += SPM_PAGESIZE;							//modify Flash page address
					bufptr = 0;
				}
				
				last_block = data[1];				
			}			
						
			tmpcnt++;
		}else{
			cc = 0x82;
		}
				
		return cc;
}
					
unsigned char flash_check(unsigned int length){
		
	unsigned int i;
	unsigned char completion_code;
		
	if (bufptr > 0){
		for(i=bufptr; i < SPM_PAGESIZE; i++)
			buf[i] = 0xff;
		boot_program_page (FlashAddr, buf);           //write in progress buffer to Flash
	}
		
	if(length != image_size)
		return 0x81;
	else
		return 0x00;
	
	return completion_code;
}

//update one Flash page
unsigned char boot_program_page (uint32_t page, uint8_t *buf){
    uint16_t i;
    
	if (page >= 0x1E000){
		return 0x81;
	}
	
    eeprom_busy_wait ();

    boot_page_erase (page);
    boot_spm_busy_wait ();      // Wait until the memory is erased.

    for (i=0; i<SPM_PAGESIZE; i+=2)
    {
        // Set up little-endian word.

        uint16_t w = *buf++;
        w += (*buf++) << 8;
        
        boot_page_fill (page + i, w);
    }

    boot_page_write (page);     // Store buffer in flash page.
    boot_spm_busy_wait();       // Wait until the memory is written.

    // Reenable RWW-section again. We need this if we want to jump back
    // to the application after bootloading.
    boot_rww_enable ();
	
	return 0x00;
}