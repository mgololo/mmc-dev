/*
 * project.h
 *
 * Created: 01/07/2015 18:39:08
 *  Author: jumendez
 */ 


#ifndef PROJECT_H_
#define PROJECT_H_

//I2C buffer size for IPMB messages
#define MAX_BYTES_READ 60

#define FALSE	0
#define TRUE	1

//Supported IPMI release, nibble swapped
#define MMC_IPMI_REL		0x51	// V1.5

//Additional device support
#define IPMI_MSG_ADD_DEV_SUPP	0x29	// event receiver, accept sensor cmds

#define AMC		0x00
#define RTM		0x01

#define DEACTIVATED		0
#define ACTIVATED		1
#define PRESENT			2
#define COMPATIBLE		3
#define NON_COMPATIBLE	4
#define NOT_PRESENT		5
#define ACTIVATION		6

#define PROGRAM_START	AVR32_FLASH_ADDRESS+0x9000

#endif /* PROJECT_H_ */