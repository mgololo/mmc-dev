/*
 * timer.c
 *
 * Created: 16/10/2015 09:15:30
 *  Author: jumendez
 */ 
#include <asf.h>

void delay_ms(unsigned short time_ms){
	cpu_delay_ms(time_ms,CONFIG_PLL0_MUL*BOARD_OSC0_HZ);
}

void delay_us(unsigned short time_us){
	cpu_delay_us(time_us,CONFIG_PLL0_MUL*BOARD_OSC0_HZ);
}