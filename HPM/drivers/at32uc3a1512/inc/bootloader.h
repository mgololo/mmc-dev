/*
 * bootloader.h
 *
 * Created: 20/10/2014 10:02:15
 *  Author: jumendez
 */ 


#ifndef BOOTLOADER_H_
#define BOOTLOADER_H_

#define MEMORY_SIZE		   512//E2END //JM: Erasing all the memory take long time. I limit the size to 512 bytes

void flash_init();
unsigned char writeflash(unsigned int addr, unsigned char *ptr, unsigned int size);

#endif /* BOOTLOADER_H_ */