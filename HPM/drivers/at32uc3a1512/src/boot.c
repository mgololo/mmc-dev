/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>
#include "../inc/i2c.h"
#include "../inc/io.h"
#include "../inc/bootloader.h"

#include "../../drivers.h"

void osc_init() {
  unsigned long mask;

  // initalize all timebases on the system
  AVR32_PM.OSCCTRL0.mode = AVR32_PM_OSCCTRL0_MODE_CRYSTAL_G3;
  AVR32_PM.OSCCTRL0.startup = AVR32_PM_OSCCTRL0_STARTUP_2048_RCOSC;
  AVR32_PM.MCCTRL.osc0en = 1;										// enable oscillator 0
  while (!(AVR32_PM.poscsr & AVR32_PM_POSCSR_OSC0RDY_MASK));			// wait for osc0 to be stable and ready

  // initialize the 32 kHz oscillator
  AVR32_PM.OSCCTRL32.mode = AVR32_PM_OSCCTRL32_MODE_CRYSTAL;
  AVR32_PM.OSCCTRL32.startup = AVR32_PM_OSCCTRL32_STARTUP_8192_RCOSC;
  AVR32_PM.OSCCTRL32.osc32en = 1;

  //DEBUG:  uncomment these next two lines to bypass the PLL and run everything
  //  (CPU, PBA, PBB, HSB) off of the 12 MHz CPU clock
  //AVR32_PM.MCCTRL.mcsel = 1;
  //return;

  // initialize PLL0 to 4x multiplication (8x multiplication at VCO)
  volatile avr32_pm_pll_t* pPLL = &(AVR32_PM.PLL[0]);
  pPLL->pllosc = 0;											// select oscillator 0
  pPLL->pllcount = 0x10;
  pPLL->plldiv = 0;

  pPLL->pllmul = 3;										// fVCO = 2*(PLLMULT+1)*fOSC
  pPLL->pllopt = 3;				// wide bandwidth mode, fPLL = fVCO/2, lower VCO freq range
  pPLL->pllen = 1;											// enable PLL
  while (!(AVR32_PM.poscsr & AVR32_PM_POSCSR_LOCK0_MASK));		// wait for PLL to lock

  // set the PBA/PBB clocks at 1/4 the main clock
  mask = (1 << AVR32_PM_CKSEL_PBBDIV_OFFSET) |
    (1 << AVR32_PM_CKSEL_PBADIV_OFFSET) |
    (1 << AVR32_PM_CKSEL_PBBSEL_OFFSET) |
    (1 << AVR32_PM_CKSEL_PBASEL_OFFSET);
  AVR32_PM.cksel = mask;

  AVR32_PM.MCCTRL.mcsel = 2;								// set PLL0 to be the main clock frequency

  // turn off clocks to USB
  AVR32_PM.hsbmask &= ~(0x8);								// USB on HSB
  AVR32_PM.pbbmask &= ~(0x2);
}


void boot_uC (void)
{
	/* Insert system clock initialization code here (sysclk_init()). */
	irq_initialize_vectors();
	
	//osc_init(); 
	sysclk_init();
	
	sleepmgr_init();
	flash_init();
	init_port();
	i2cInit();
	
	cpu_irq_enable();
}

void reset_uC(){
	reset_do_soft_reset();
}

void vbus_event(unsigned char b_vbus_high){
	if(b_vbus_high)	udc_attach();
	else			udc_detach();
}
