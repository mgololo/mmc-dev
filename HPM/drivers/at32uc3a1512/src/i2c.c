/*
 * i2c.c
 *
 * Created: 09/11/2015 11:49:53
 *  Author: jumendez
 */
#include <asf.h> 

#include "../../drivers.h"
#include "../inc/i2c.h"

//#include "../../../application/inc/led.h"
#include "../../../application/inc/ipmi_if.h"

/*****************************************
 GLOBAL VARIABLES
******************************************/
// receive buffer (incoming data)
static unsigned char I2cReceiveData[MAX_BYTES_READ+15];
static unsigned char I2cReceiveDataIndex=1;

unsigned char i2cBusy = 0;

// function pointer to i2c receive routine
// I2cSlaveReceive is called when this processor is addressed as a slave for writing
static void (*i2cSlaveReceive)(unsigned char receiveDataLength, unsigned char* recieveData);
//static void (*i2cSlaveTransmit)(unsigned char receiveDataLength, unsigned char* recieveData);

static twi_master_options_t TWI_MASTER_OPTS = {
	.speed = 100000,
	.chip  = 0
};

static twi_slave_fct_t TWI_CBS =
{
	.rx = &i2c_slave_rx,
	.tx = &i2c_slave_tx,
	.stop = &i2c_slave_stop
};

static twi_options_t TWI_SLAVE_OPTS =
{
	.speed = 100000,
	.chip = 0 // Set @ Runtime
};

/*****************************************
 INIT FUNCTIONS
******************************************/
void i2cInit(){	
	gpio_enable_module_pin(AVR32_TWI_SDA_0_0_PIN, AVR32_TWI_SDA_0_0_FUNCTION);
	gpio_enable_module_pin(AVR32_TWI_SCL_0_0_PIN, AVR32_TWI_SCL_0_0_FUNCTION);
	
	TWI_SLAVE_OPTS.chip = get_address() >> 1;
	TWI_MASTER_OPTS.chip = get_address() >> 1;
	
	AVR32_TWI.CR.swrst = 0x1;
	AVR32_TWI.CR.stop = 0x1;
	
	//twi_master_setup(&AVR32_TWI, &TWI_MASTER_OPTS);
	twi_slave_setup( &AVR32_TWI, &TWI_SLAVE_OPTS, &TWI_CBS);
}

/*****************************************
 DRIVER FUNCTIONS
******************************************/
void ipmb_i2c_configuration(unsigned char address, void (*ipmb_callback)(unsigned char receiveDataLength, unsigned char* recieveData)){
	i2cSlaveReceive = ipmb_callback;
}

#include <string.h>

/*****************************************
 MASTER FUNCTIONS
******************************************/
void ipmb_send(unsigned char deviceAddr, unsigned char length, unsigned char *data){
			
	twi_package_t package;
	package.chip = (deviceAddr & 0xFE) >> 1;
	package.addr_length = 0;
	package.buffer = (void *)data;
	package.length = length;
	
	twi_disable_interrupt(&AVR32_TWI);
	twi_master_setup(&AVR32_TWI, &TWI_MASTER_OPTS);
	twi_master_write(&AVR32_TWI, &package);		
	
	twi_disable_interrupt(&AVR32_TWI);
	AVR32_TWI.CR.swrst = 0x1;
	AVR32_TWI.CR.stop = 0x1;
	
	twi_slave_setup( &AVR32_TWI, &TWI_SLAVE_OPTS, &TWI_CBS);
}


/*****************************************
 SLAVE FUNCTIONS
******************************************/
unsigned char nbpacketcnt = 0;

void i2c_slave_rx ( unsigned char pByteIn ){
	
	i2cBusy = 1;
	
	if ( I2cReceiveDataIndex < MAX_BYTES_READ+15 ){
		I2cReceiveData[I2cReceiveDataIndex++] = pByteIn;
	}else{
		I2cReceiveDataIndex = 1;
	}
	return;
}

unsigned char i2c_slave_tx(){
	//Not used - MMC must reply as I2C master as mentioned in the standard
	return 0xFF;
}

void i2c_slave_stop(){
	I2cReceiveData[0] = get_twis_addr() << 1;
	if(i2cSlaveReceive)
		i2cSlaveReceive(I2cReceiveDataIndex, I2cReceiveData);         // i2c receive is complete, call i2cSlaveReceive
	I2cReceiveDataIndex = 1;
	
	i2cBusy = 0;
	return;
}